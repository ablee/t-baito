<!DOCTYPE html>
<html>
<?php include ('head.php'); ?>
<body>
<div class="theme-layout" id="scrollup">
	<?php include ('responsive-header.php'); ?>
	<?php include ('header.php'); ?>
	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: transparent url(../images/resource/mslider3.jpg) repeat scroll 50% -41.3px;" class="parallax scrolly-invisible no-parallax"></div>
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="ab inner-header"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block block-now">
			<div class="category top text-center"> 
				<ul class="nav your_custom_class">
					<li>
						<a href="feature.php">Feature<span class="l"></span></a>
					</li>
					<li>
						<a class="active" href="price.php">Pricing<span class="l"></span></a>
					</li>
					<li>
						<a href="partners.php">Partners<span class="l"></span></a>
					</li>
					<li>
						<a href="support-1.php">Support<span class="l"></span></a>
					</li>
				</ul>		
			</div>
		</div>
	</section>
	<section class="bb">
		<div class="block">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="heading">
							<h2 class="mb50">Choose a plan that ﬁts your business goal</h2>
							<span>Trusted by thousands, T-baito powers hiring teams around the world. See which plan is right for you.
							</span>
						</div>
						<div class="plans-sec">
							<div class="row">
								<div class="col-lg-4">
									<div class="pricetable">
										<div class="pricetable-head">
											<h2>FREE TRIAL</h2>
											<span>¥0/month</span>
										</div>
										<ul>
											<li>Job post 1time/day </li>
											<li>Candidates 3 people/job</li> 
											<li>Push notiﬁcation - 1 time/day </li> 
										</ul>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="pricetable">
										<div class="pricetable-head">
											<h2> BASIC</h2>
											<span> ¥999/month</span>
										</div>
										<ul>
											<p>Promote jobs online and screen resumes</p>
											<li>Job post 1time/day </li>
											<li>Candidates 3 people/job</li>
											<li>Push notiﬁcation - 1 time/day  Contacting Scheduling by Chat Bot
 </li>
											<li>Candidate screening by AI </li>
											<li>Interviews & Assessments with Assistance of AI
</li>
										</ul>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="pricetable">
										<div class="pricetable-head">
											<h2> PROFESSIONAL</h2>
											<span>¥5999/month</span>
										</div>
										<ul>
											<p>Track applicants and monitor interviews</p>
											<li>Job post 1time/day</li>
											<li>Candidates 3 people/job </li>
											<li>Push notiﬁcation - 1 time/day </li>
											<li>Contacting Scheduling by Chat Bot</li>
											<li>Candidate screening by AI</li>
											<li>Interviews & Assessments with Assistance of AI</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="bb">
		<div class="block gray">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="heading">
							<h2>Plan Comparison</h2>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
						<div class="ne mb50">
							<h3>Job posting</h3>
						</div>
						<div class="no">
							<ul>
								<li>Mobile Friendly Job Board</li>
								<li>Customizable Job Applications</li>
								<li>Post Jobs to Free Job Boards</li>
								<li>Purchase Premium Job Listings</li>
								<li>New Candidate Email Alerts</li>
								<li>Mobile Resume Screener</li>
								<li>Resume Uploader & Parser</li>
								<li>Searchable Resume Database</li>
							</ul>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
						<div class="bro pricetable">
							<div class="pricetable-head">
								<h2>SILVER</h2>
							</div><!-- Price Table -->
							<div class="checkin">
								<ul>
									<li>
										<input name="spealism" id="a" type="checkbox">
										<label for="a"></label>
									</li>
									<li>
										<input name="spealism" id="b" type="checkbox">
										<label for="b"></label>
									</li>
									<li>
										<input name="spealism" id="c" type="checkbox">
										<label for="c"></label>
									</li>
									<li>
										<input name="spealism" id="d" type="checkbox">
										<label for="d"></label>
									</li>
									<li>
										<input name="spealism" id="e" type="checkbox">
										<label for="e"></label>
									</li>
									<li>
										<input name="spealism" id="f" type="checkbox">
										<label for="f"></label>
									</li>
									<li>
										<input name="spealism" id="g" type="checkbox">
										<label for="g"></label>
									</li>
									<li>
										<input name="spealism" id="h" type="checkbox">
										<label for="h"></label>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
						<div class="bro pricetable">
							<div class="pricetable-head">
								<h2>GOLD</h2>
							</div><!-- Price Table -->
							<div class="checkin">
								<ul>
									<li>
										<input name="spealism" id="a" type="checkbox">
										<label for="a"></label>
									</li>
									<li>
										<input name="spealism" id="b" type="checkbox">
										<label for="b"></label>
									</li>
									<li>
										<input name="spealism" id="c" type="checkbox">
										<label for="c"></label>
									</li>
									<li>
										<input name="spealism" id="d" type="checkbox">
										<label for="d"></label>
									</li>
									<li>
										<input name="spealism" id="e" type="checkbox">
										<label for="e"></label>
									</li>
									<li>
										<input name="spealism" id="f" type="checkbox">
										<label for="f"></label>
									</li>
									<li>
										<input name="spealism" id="g" type="checkbox">
										<label for="g"></label>
									</li>
									<li>
										<input name="spealism" id="h" type="checkbox">
										<label for="h"></label>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
						<div class="bro pricetable">
							<div class="pricetable-head">
								<h2>PLATINUM</h2>
							</div><!-- Price Table -->
							<div class="checkin">
								<ul>
									<li>
										<input name="spealism" id="a" type="checkbox">
										<label for="a"></label>
									</li>
									<li>
										<input name="spealism" id="b" type="checkbox">
										<label for="b"></label>
									</li>
									<li>
										<input name="spealism" id="c" type="checkbox">
										<label for="c"></label>
									</li>
									<li>
										<input name="spealism" id="d" type="checkbox">
										<label for="d"></label>
									</li>
									<li>
										<input name="spealism" id="e" type="checkbox">
										<label for="e"></label>
									</li>
									<li>
										<input name="spealism" id="f" type="checkbox">
										<label for="f"></label>
									</li>
									<li>
										<input name="spealism" id="g" type="checkbox">
										<label for="g"></label>
									</li>
									<li>
										<input name="spealism" id="h" type="checkbox">
										<label for="h"></label>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="bb">
		<div class="block gray">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
						<div class="ne mb50">
							<h3>Interviews & Assessment</h3>
						</div>
						<div class="no">
							<ul>
								<li>Background Checks</li>
								<li>Recruiting Calendar</li>
								<li>Gmail & Outlook </li>
								<li>Calendar</li>
								<li>Sync</li>
								<li>Interview Guides</li>
								<li>Candidate Evaluation</li>
								<li>Templates</li>
								<li>Video Interview </li>
							</ul>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
						<div class="bro pricetable">
							<div class="pricetable-head">
								<h2>Silver</h2>
							</div><!-- Price Table -->
							<div class="checkin">
								<ul>
									<li>
										<input name="spealism" id="c" type="checkbox">
										<label for="c"></label>
									</li>
									<li>
										<input name="spealism" id="d" type="checkbox">
										<label for="d"></label>
									</li>
									<li>
										<input name="spealism" id="e" type="checkbox">
										<label for="e"></label>
									</li>
									<li>
										<input name="spealism" id="f" type="checkbox">
										<label for="f"></label>
									</li>
									<li>
										<input name="spealism" id="g" type="checkbox">
										<label for="g"></label>
									</li>
									<li>
										<input name="spealism" id="h" type="checkbox">
										<label for="h"></label>
									</li>
									<li>
										<input name="spealism" id="i" type="checkbox">
										<label for="i"></label>
									</li>
									<li>
										<input name="spealism" id="j" type="checkbox">
										<label for="j"></label>
									</li>
									<li>
										<input name="spealism" id="k" type="checkbox">
										<label for="k"></label>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
						<div class="bro pricetable">
							<div class="pricetable-head">
								<h2>GOLD</h2>
							</div><!-- Price Table -->
							<div class="checkin">
								<ul>
									<li>
										<input name="spealism" id="c" type="checkbox">
										<label for="c"></label>
									</li>
									<li>
										<input name="spealism" id="d" type="checkbox">
										<label for="d"></label>
									</li>
									<li>
										<input name="spealism" id="e" type="checkbox">
										<label for="e"></label>
									</li>
									<li>
										<input name="spealism" id="f" type="checkbox">
										<label for="f"></label>
									</li>
									<li>
										<input name="spealism" id="g" type="checkbox">
										<label for="g"></label>
									</li>
									<li>
										<input name="spealism" id="h" type="checkbox">
										<label for="h"></label>
									</li>
									<li>
										<input name="spealism" id="i" type="checkbox">
										<label for="i"></label>
									</li>
									<li>
										<input name="spealism" id="j" type="checkbox">
										<label for="j"></label>
									</li>
									<li>
										<input name="spealism" id="k" type="checkbox">
										<label for="k"></label>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
						<div class="bro pricetable">
							<div class="pricetable-head">
								<h2>Platinum</h2>
							</div><!-- Price Table -->
							<div class="checkin">
								<ul>
									<li>
										<input name="spealism" id="c" type="checkbox">
										<label for="c"></label>
									</li>
									<li>
										<input name="spealism" id="d" type="checkbox">
										<label for="d"></label>
									</li>
									<li>
										<input name="spealism" id="e" type="checkbox">
										<label for="e"></label>
									</li>
									<li>
										<input name="spealism" id="f" type="checkbox">
										<label for="f"></label>
									</li>
									<li>
										<input name="spealism" id="g" type="checkbox">
										<label for="g"></label>
									</li>
									<li>
										<input name="spealism" id="h" type="checkbox">
										<label for="h"></label>
									</li>
									<li>
										<input name="spealism" id="i" type="checkbox">
										<label for="i"></label>
									</li>
									<li>
										<input name="spealism" id="j" type="checkbox">
										<label for="j"></label>
									</li>
									<li>
										<input name="spealism" id="k" type="checkbox">
										<label for="k"></label>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="bb">
		<div class="block gray">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
						<div class="ne mb50">
							<h3>Reporting</h3>
						</div>
						<div class="no">
							<ul>
								<li>Candidate Reports</li>
								<li>Job Reports</li>
							</ul>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
						<div class="bro pricetable">
							<div class="pricetable-head">
								<h2>Silver</h2>
							</div><!-- Price Table -->
							<div class="checkin">
								<ul>
									<li>
										<input name="spealism" id="a" type="checkbox">
										<label for="a"></label>
									</li>
									<li>
										<input name="spealism" id="b" type="checkbox">
										<label for="b"></label>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
						<div class="bro pricetable">
							<div class="pricetable-head">
								<h2>Gold</h2>
							</div><!-- Price Table -->
							<div class="checkin">
								<ul>
									<li>
										<input name="spealism" id="a" type="checkbox">
										<label for="a"></label>
									</li>
									<li>
										<input name="spealism" id="b" type="checkbox">
										<label for="b"></label>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
						<div class="bro pricetable">
							<div class="pricetable-head">
								<h2>Platinum</h2>
							</div><!-- Price Table -->
							<div class="checkin">
								<ul>
									<li>
										<input name="spealism" id="a" type="checkbox">
										<label for="a"></label>
									</li>
									<li>
										<input name="spealism" id="b" type="checkbox">
										<label for="b"></label>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="bb">
		<div class="block gray">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
						<div class="ne mb50">
							<h3>Talent network</h3>
						</div>
						<div class="no">
							<ul>
								<li>Candidates</li>
								<li>Proﬁling the Ideal </li>
								<li>Candidate</li>
								<li>Recruiting More </li>
								<li>Efﬁciently</li>
								<li>Making Competitive Offers</li>
							</ul>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
						<div class="bro pricetable">
							<div class="pricetable-head">
								<h2>Silver</h2>
							</div><!-- Price Table -->
							<div class="checkin">
								<ul>
									<li>
										<input name="spealism" id="b" type="checkbox">
										<label for="b"></label>
									</li>
									<li>
										<input name="spealism" id="c" type="checkbox">
										<label for="c"></label>
									</li>
									<li>
										<input name="spealism" id="d" type="checkbox">
										<label for="d"></label>
									</li>
									<li>
										<input name="spealism" id="e" type="checkbox">
										<label for="e"></label>
									</li>
									<li>
										<input name="spealism" id="e" type="checkbox">
										<label for="e"></label>
									</li>
									<li>
										<input name="spealism" id="e" type="checkbox">
										<label for="e"></label>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
						<div class="bro pricetable">
							<div class="pricetable-head">
								<h2>Gold</h2>
							</div><!-- Price Table -->
							<div class="checkin">
								<ul>
									<li>
										<input name="spealism" id="b" type="checkbox">
										<label for="b"></label>
									</li>
									<li>
										<input name="spealism" id="c" type="checkbox">
										<label for="c"></label>
									</li>
									<li>
										<input name="spealism" id="d" type="checkbox">
										<label for="d"></label>
									</li>
									<li>
										<input name="spealism" id="e" type="checkbox">
										<label for="e"></label>
									</li>
									<li>
										<input name="spealism" id="e" type="checkbox">
										<label for="e"></label>
									</li>
									<li>
										<input name="spealism" id="e" type="checkbox">
										<label for="e"></label>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
						<div class="bro pricetable">
							<div class="pricetable-head">
								<h2>Platinum</h2>
							</div><!-- Price Table -->
							<div class="checkin">
								<ul>
									<li>
										<input name="spealism" id="b" type="checkbox">
										<label for="b"></label>
									</li>
									<li>
										<input name="spealism" id="c" type="checkbox">
										<label for="c"></label>
									</li>
									<li>
										<input name="spealism" id="d" type="checkbox">
										<label for="d"></label>
									</li>
									<li>
										<input name="spealism" id="e" type="checkbox">
										<label for="e"></label>
									</li>
									<li>
										<input name="spealism" id="e" type="checkbox">
										<label for="e"></label>
									</li>
									<li>
										<input name="spealism" id="e" type="checkbox">
										<label for="e"></label>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="bb">
		<div class="block gray">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
						<div class="ne mb50">
							<h3>Support</h3>
						</div>
						<div class="no">
							<ul>
								<li>Assistance</li>
								<li>Data Import</li>
								<li>Customer  Support</li>
							</ul>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
						<div class="bro pricetable">
							<div class="pricetable-head">
								<h2>Silver</h2>
							</div><!-- Price Table -->
							<div class="checkin">
								<ul>
									<li>
										<input name="spealism" id="b" type="checkbox">
										<label for="b"></label>
									</li>
									<li>
										<input name="spealism" id="c" type="checkbox">
										<label for="c"></label>
									</li>
									<li>
										<input name="spealism" id="d" type="checkbox">
										<label for="d"></label>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
						<div class="bro pricetable">
							<div class="pricetable-head">
								<h2>Gold</h2>
							</div><!-- Price Table -->
							<div class="checkin">
								<ul>
									<li>
										<input name="spealism" id="b" type="checkbox">
										<label for="b"></label>
									</li>
									<li>
										<input name="spealism" id="c" type="checkbox">
										<label for="c"></label>
									</li>
									<li>
										<input name="spealism" id="d" type="checkbox">
										<label for="d"></label>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
						<div class="bro pricetable">
							<div class="pricetable-head">
								<h2>Platinum</h2>
							</div><!-- Price Table -->
							<div class="checkin">
								<ul>
									<li>
										<input name="spealism" id="b" type="checkbox">
										<label for="b"></label>
									</li>
									<li>
										<input name="spealism" id="c" type="checkbox">
										<label for="c"></label>
									</li>
									<li>
										<input name="spealism" id="d" type="checkbox">
										<label for="d"></label>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="bb">
		<div class="block double-gap-top double-gap-bottom">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="simple-text-block eq">
							<h3>Get growing with the plan that is right for your business.</h3>
							<span style="color:#666">
							Custom plans are available for HR service providers, stafﬁng or employment agencies, franchises, multi-location businesses, and organizations with over 500 employees. For detailed pricing information or custom plans, please contact us.</span>
							<a href="#" title="">Phone: 070 3193 7965</a>
							<a href="#" title="">Company@t-baito.com</a>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</section>
	<?php include ('footer.php'); ?>
</div>
<?php include ('foot.php'); ?>
</body>
</html>
