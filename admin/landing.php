<!DOCTYPE html>
<html>
<?php include ('head.php'); ?>
<body>
<div class="theme-layout" id="scrollup">
	<?php include ('responsive-header.php'); ?>
	<?php include ('header.php'); ?>
	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: transparent url(../images/resource/mslider3.jpg) repeat scroll 50% -41.3px;" class="parallax scrolly-invisible no-parallax"></div>
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="ab inner-header"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block block-now">
			<div class="category top text-center"> 
				<ul class="nav your_custom_class">
					<li>
						<a href="feature.php">Feature<span class="l"></span></a>
					</li>
					<li>
						<a href="price.php">Pricing<span class="l"></span></a>
					</li>
					<li>
						<a href="partners.php">Partners<span class="l"></span></a>
					</li>
					<li>
						<a href="support-1.php">Support<span class="l"></span></a>
					</li>
				</ul>		
			</div>
		</div>
	</section>
	<section>
		<div class="block double-gap-top double-gap-bottom gray">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="simple-text-block">
							<h3>A fast and effective way to recruit right candidate for your company</h3>
							<a href="#" title="">Free trial</a>
							<a href="#" title="">Posting job</a>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</section>
	<section>
		<div class="block">
			<div data-velocity="-.1" style="background: transparent url(../images/resource/parallax1.jpg) repeat scroll 50% -17.95px;" class="parallax scrolly-invisible layer color"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="heading">
							<h2 style="color:#fff;font-size: 22px;"> Candidate outsourcing solutions you need for well-organized recruitment and management processes </h2>
						</div>
					</div>
				</div>
				<div class="row help double-gap-top">
					<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="b">
							<div class="icons">
								<i class="la la-bullhorn"></i>
							</div>
							<div style="color:#fff" class="description">
								Get Best Candidate?<br>
								<a href="#">GO TO BASIC PLAN</a>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="b">
							<div class="icons">
								<i class="la la-graduation-cap"></i>
							</div>
							<div style="color:#fff" class="description">
								Manage Applicants?<br>
								<a href="#">GO TO PROFESSIONAL PLAN</a>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div>
				</div>
			</div>	
		</div>
	</section>
	<section>
		<div class="block gray">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="cat-sec">
							<div class="row no-gape">
								<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
									<div class="p-category green">
										<a href="#" title="">
											<img src="../images/resource/icon-1.png" />
											<span>COMMUNICATION</span>
											<p>Create talent network with up-to-date HR data tools .</p>
										</a>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
									<div class="p-category green">
										<a href="#" title="">
											<img src="../images/resource/icon-1.png" />
											<span>GROUP CHAT</span>
											<p>Connect and stay in touch with </br> staffs</p>
											<div class="more">Learn more</div>
										</a>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
									<div class="p-category green">
										<a href="#" title="">
											<img src="../images/resource/icon-1.png" />
											<span>EVENT</span>
											<p>Organize all interview steps with best tools in hands</p>
											<div class="more">Learn more</div>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="cat-sec">
							<div class="row no-gape">
								<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
									<div class="p-category green">
										<a href="#" title="">
											<img src="../images/resource/icon-1.png" />
											<span>RECRUITMENT </span>
											<p>Manage the entire recruitment processes in organized and simple way.</p>
											<div class="more">Learn more</div>
										</a>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
									<div class="p-category green">
										<a href="#" title="">
											<img src="../images/resource/icon-1.png" />
											<span>SHIFT MANAGEMENT</span>
											<p>Organize time-sheets from staffs easily and timely</p>
											<div class="more">Learn more</div>
										</a>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
									<div class="p-category green">
										<a href="#" title="">
											<img src="../images/resource/icon-1.png" />
											<span>FINANCE MANAGEMENT</span>
											<p>Provide tools for HR professionals manage ﬁnancial reports </p>
											<div class="more">Learn more</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="browse-all-cat green">
							<a href="#" title="" class="">TAKE A PRODUCT TOUR</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block">
			<div data-velocity="-.1" style="background: transparent url(../images/resource/mslider3.jpg) repeat scroll 50% -17.95px;" class="parallax scrolly-invisible layer color"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="heading">
							<h2 style="color:#fff;margin-bottom: 100px">Companies face Recruitment and HR Issues in Japanese Labor Market
							</h2>
						</div>
					</div>
					<div class="col-lg-4 text-center">
						<h5 style="color:#fff">Young labor shortage</h5>
						<span style="color:#fff">Aging population, in need of foreigners for satifying labor force
						</span>
					</div>
					<div class="col-lg-4 text-center">
						<h5 style="color:#fff">Complicated recruitment process</h5>
						<span style="color:#fff">Time and money consuming recruitment methods, complicated procedures of Japanese working culture</span>
					</div>
					<div class="col-lg-4 text-center">
						<h5 style="color:#fff">Foreigners facing job-hunting issues</h5>
						<span style="color:#fff">Millions of jobs online and easy to ﬁnd and apply via mobile apps.</span>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="heading">
							<h2 style="color:#fff;margin-bottom: 50px;margin-top: 175px">T-baito provides comprehensive recruiting solutions</h2>
						</div>
					</div>
					<div class="col-lg-4 text-center">
						<h5 style="color:#fff">Make your job ads public</h5>
						<span style="color:#fff">Promote jobs online and screen resumes
						</span>
						<div class="browse-all-cat green">
							<a style="color:#fff" href="#" title="" class="">Silver Client</a>
						</div>
					</div>
					<div class="col-lg-4 text-center">
						<h5 style="color:#fff">ATS system</h5>
						<span style="color:#fff">Track applicants and manage interviews</span>
						<div class="browse-all-cat green">
							<a style="color:#fff" href="#" title="" class=""> Gold Client</a>
						</div>
					</div>
					<div class="col-lg-4 text-center">
						<h5 style="color:#fff">HR management</h5>
						<span style="color:#fff">Develop talent network</span>
						<div class="browse-all-cat green">
							<a style="color:#fff" href="#" title="" class=""> Platinum</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block double-gap-top double-gap-bottom gray">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="simple-text-block">
							<h3>Explore why our clients choose T-Baito?</h3>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="video text-center" style="background-image: url(../images/resource/parallax1.jpg);">
							<a class="video-popup video-play-popup">
								<i class="la la-play"></i>
							</a>
						</div>
					</div>
				</div>
				<div class="row" id="team-carousel">
					<div class="col-lg-3">
						<div class="top-compnay style2">
							<img src="../images/resource/cc1.jpg" alt="" />
							<h3><a href="#" title="">Logo of Partner</a></h3>
						</div><!-- Top Company -->
					</div>
					<div class="col-lg-3">
						<div class="top-compnay style2">
							<img src="../images/resource/cc2.jpg" alt="" />
							<h3><a href="#" title="">Logo of Partner</a></h3>
						</div><!-- Top Company -->
					</div>
					<div class="col-lg-3">
						<div class="top-compnay style2">
							<img src="../images/resource/cc3.jpg" alt="" />
							<h3><a href="#" title="">Logo of Partner</a></h3>
						</div><!-- Top Company -->
					</div>
					<div class="col-lg-3">
						<div class="top-compnay style2">
							<img src="../images/resource/cc4.jpg" alt="" />
							<h3><a href="#" title="">Logo of Partner</a></h3>
						</div><!-- Top Company -->
					</div>
					<div class="col-lg-3">
						<div class="top-compnay style2">
							<img src="../images/resource/cc5.jpg" alt="" />
							<h3><a href="#" title="">Logo of Partner</a></h3>
						</div><!-- Top Company -->
					</div>
					<div class="col-lg-3">
						<div class="top-compnay style2">
							<img src="../images/resource/cc1.jpg" alt="" />
							<h3><a href="#" title="">Logo of Partner</a></h3>
						</div><!-- Top Company -->
					</div>
					<div class="col-lg-3">
						<div class="top-compnay style2">
							<img src="../images/resource/cc2.jpg" alt="" />
							<h3><a href="#" title="">Logo of Partner</a></h3>
						</div><!-- Top Company -->
					</div>
				</div>
			</div>	
		</div>
	</section>
	<section>
		<div class="block double-gap-top double-gap-bottom">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="simple-text-block heading">
							<h3>Try it for free</h3>
							<span style="color:#666">
								See why thousands of companies rely on T-baito’s applicant tracking system to optimize and improve their recruiting process
							</span>
							<a href="#" title="">Post your ﬁrst Job</a>
							<a href="#" title="">View Pricing</a>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</section>
	<section>
		<div class="block gray">
			<div class="container">
				 <div class="row">
				 	<div class="col-lg-2"></div>
				 	<div class="col-lg-8">
				 		<div class="about-us">
				 			<div class="row">
				 				<div class="col-lg-12">
				 					<div class="contact-form">
				 						<h3>Contact us</h3>
							 			<form>
							 				<div class="row">
							 					<div class="col-lg-12">
							 						<div class="pf-field">
							 							<input placeholder="Enter your name" type="text">
							 						</div>
							 					</div>
							 					<div class="col-lg-12">
							 						<div class="pf-field">
							 							<input placeholder="E-mail address" type="text">
							 						</div>
							 					</div>
							 					<div class="col-lg-12">
							 						<div class="pf-field">
							 							<input placeholder="Phone number" type="text">
							 						</div>
							 					</div>
							 					<div class="col-lg-12">
							 						<div class="pf-field">
							 							<textarea placeholder="Message should have more than 50 characters"></textarea>
							 						</div>
							 					</div>
							 					<div class="col-lg-12">
							 						<button type="submit">Send</button>
							 					</div>
							 				</div>
							 			</form>
							 		</div>
				 				</div>
				 			</div>
				 		</div>
				 	</div>
				 	<div class="col-lg-2"></div>
				 </div>
			</div>
		</div>
	</section>
	<?php include ('footer.php'); ?>
</div>
<?php include ('foot.php'); ?>
</body>
</html>
