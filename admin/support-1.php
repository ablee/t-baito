<!DOCTYPE html>
<html>
<?php include ('head.php'); ?>
<body>
<div class="theme-layout" id="scrollup">
	<?php include ('responsive-header.php'); ?>
	<?php include ('header.php'); ?>
	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: transparent url(../images/resource/mslider3.jpg) repeat scroll 50% -41.3px;" class="parallax scrolly-invisible no-parallax"></div>
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="ab inner-header"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block block-now">
			<div class="category top text-center"> 
				<ul class="nav your_custom_class">
					<li>
						<a href="feature.php">Feature<span class="l"></span></a>
					</li>
					<li>
						<a href="price.php">Pricing<span class="l"></span></a>
					</li>
					<li>
						<a href="partners.php">Partners<span class="l"></span></a>
					</li>
					<li>
						<a class="active" href="support-1.php">Support<span class="l"></span></a>
					</li>
				</ul>		
			</div>
		</div>
	</section>
	<section>
		<div class="block double-gap-top double-gap-bottom">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="heading">
							<h2>How can we help?</h2>
							<span>T-Baito puts all your customer support interactions in one place, so communication is seamless, personal, and efﬁcient–which means more productive agents and satisﬁed customers.
							</span>
						</div><!-- Heading -->
						<div class="how-to-sec style2">
							<div class="how-to">
								<span class="how-icon"><i class="la la-user"></i></span>
								<h3>Lead customers to happiness</h3>
								<p>Give customers what they want – quick and easy resolutions to their issues. T-Baito helps you provide personalized support when and where they need it, so customers stay happy.</p>
							</div>
							<div class="how-to">
								<span class="how-icon"><i class="la la-file-archive-o"></i></span>
								<h3>Support your support</h3>
								<p>Productive agents are happy agents. Give them all the support tools and information they need to best serve your customers.</p>
							</div>
							<div class="how-to">
								<span class="how-icon"><i class="la la-list"></i></span>
								<h3>Grow without growing pains</h3>
								<p>T-baito lets you customize your support and conﬁgure any workﬂow. Our software is powerful enough to handle the most complex business, yet ﬂexible enough to scale with you as you grow.</p>
							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="browse-all-cat green">
							<a href="#" title="" class="rounded">Try it for free</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block double-gap-top double-gap-bottom">
			<div data-velocity="-.1" style="background: transparent url(../images/resource/parallax1.jpg) repeat scroll 50% -54.89px;" class="parallax scrolly-invisible layer color red"></div>
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<div class="aeq text-left">
							<h2>Customize and personalize</h2>
							<span>T-Baito is designed to be ﬂexible. It works right out of the box, or it can be conﬁgured to your preferences. Customize a workﬂow or use apps and integrations–any way you use it, T-baito has the ﬂexibility to ﬁt your support needs.
							</span>
						</div>
					</div>
					<div class="col-lg-6">
						
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block double-gap-top double-gap-bottom">
			<div data-velocity="-.1" style="background: transparent url(../images/resource/mslider2.jpg) repeat scroll 50% -54.89px;" class="parallax scrolly-invisible layer color red"></div>
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						
					</div>
					<div class="col-lg-6">
						<div class="aeq text-left">
							<h2>Be smart about your support </h2>
							<span>T-Baito arms you with insights so that you can measure and improve your customer service. Learn what your customers think through customer satisfaction ratings and get analytics on how you’re doing through performance reports and dashboards
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block double-gap-top double-gap-bottom">
			<div data-velocity="-.1" style="background: transparent url(../images/resource/mslider3.jpg) repeat scroll 50% -54.89px;" class="parallax scrolly-invisible layer color red"></div>
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<div class="aeq text-left">
							<span>“The usability and ease of managing T-baito has made it easy for me to serve our customers. We’re proud to have one of the highest customer ratings you can imagine.”</span>
						</div>
					</div>
					<div class="col-lg-6">
						
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php include ('footer.php'); ?>
</div>
<?php include ('foot.php'); ?>
</body>
</html>
