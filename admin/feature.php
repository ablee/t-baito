<!DOCTYPE html>
<html>
<?php include ('head.php'); ?>
<body>
<div class="theme-layout" id="scrollup">
	<?php include ('responsive-header.php'); ?>
	<?php include ('header.php'); ?>
	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: transparent url(../images/resource/mslider3.jpg) repeat scroll 50% -41.3px;" class="parallax scrolly-invisible no-parallax"></div>
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="ab inner-header"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block block-now">
			<div class="category top text-center"> 
				<ul class="nav your_custom_class">
					<li>
						<a class="active" href="feature.php">Feature<span class="l"></span></a>
					</li>
					<li>
						<a href="price.php">Pricing<span class="l"></span></a>
					</li>
					<li>
						<a href="partners.php">Partners<span class="l"></span></a>
					</li>
					<li>
						<a href="support-1.php">Support<span class="l"></span></a>
					</li>
				</ul>		
			</div>
		</div>
	</section>
	<section>
		<div class="block double-gap-top double-gap-bottom gray">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="simple-text-block">
							<h3>A Recruitment System Tailored by and for All HR Professionals</h3>
							<a href="#" title="">Request a Trial</a>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</section>
	<section>
		<div class="block ">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="simple-text-block mb50">
							<h3>AI</h3>
						</div>
						<div class="how-works flip">
							<div class="how-workimg"><img src="../images/resource/parallax1.jpg" alt=""></div>
							<div class="how-work-detail">
								<div class="how-work-box">
									<h3>AI helps candidate screening</h3>
									<p>Automating applications screening and ranking tasks for recruiters make the best decision 
 </p>
									<a style="color:#fff" class="in"href="#">JOB POSTING & SYNDICATION</a>
									</br></br>
									<h3>AI assists Scheduling</h3>
									<p>Enabling HR professionals make schedule with candidates easier and faster on the go using our mobile app T-Baito for managers</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block ">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="simple-text-block mb50">
							<h3>ChatBot</h3>
						</div>
						<div class="how-works">
							<div class="how-workimg"><img src="../images/resource/parallax2.jpg" alt=""></div>
							<div class="how-work-detail">
								<div class="how-work-box">
									<h3>Stay in Touch with candidates</h3>
									<p>Keep your potential candidates in hand by intuitive chat UI window a</p>
									<a style="color:#fff" class="in"href="#">JOB POSTING & SYNDICATION</a>
									</br></br>
									<h3>Manage Staffs instantly</h3>
									<p>Receive timesheet, notiﬁcations, schedule new messages, and set auto-replies to candidates and staffs</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block ">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="simple-text-block mb50">
							<h3>Tracking and Management</h3>
						</div>
						<div class="how-works flip">
							<div class="how-workimg"><img class="last" src="../images/resource/parallax1.jpg" alt=""></div>
							<div class="how-work-detail">
								<div class="how-work-box">
									<h3>Broad network of candidates</h3>
									<p style="margin-bottom: 20px;">Keep and enlarge your professional network /p>
									</br></br></br></br>
									<h3>Promote your business</h3>
									<p>Share your network and gain important recruiting insights with the other companies also using our system</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php include ('footer.php'); ?>
</div>
<?php include ('foot.php'); ?>
</body>
</html>
