<!DOCTYPE html>
<html>
<?php include ('head.php'); ?>
<body>
<div class="theme-layout" id="scrollup">
	<?php include ('responsive-header.php'); ?>
	<?php include ('header.php'); ?>
	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: transparent url(../images/resource/mslider3.jpg) repeat scroll 50% -41.3px;" class="parallax scrolly-invisible no-parallax"></div>
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="ab inner-header"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block block-now">
			<div class="category top text-center"> 
				<ul class="nav your_custom_class">
					<li>
						<a href="feature.php">Feature<span class="l"></span></a>
					</li>
					<li>
						<a href="price.php">Pricing<span class="l"></span></a>
					</li>
					<li>
						<a class="active" href="partners.php">Partners<span class="l"></span></a>
					</li>
					<li>
						<a href="support-1.php">Support<span class="l"></span></a>
					</li>
				</ul>		
			</div>
		</div>
	</section>
	<section>
		<div class="block double-gap-top double-gap-bottom gray">
			<div class="container">
				<div class="row">
					<div class="col-lg-4">
						<div class="simple-text-block">
							<h3>T-BAITO ATS PARTNER PROGRAM</h3>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="simple-text-block">
							<a class="fill-again s" href="#" title="">APPLY TODAY</a>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="simple-text-block">
							<a class="s" href="#" title=""> CONTACT TEAM</a>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</section>
	<section>
		<div class="block double-gap-top double-gap-bottom">
			<div data-velocity="-.1" style="background: transparent url(../images/resource/mslider3.jpg) repeat scroll 50% -17.95px;" class="parallax scrolly-invisible layer color"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="simple-text-block">
							<h3 style="color:#fff">Promote your client base and your business</h3>
							<span>We bring powerful ATS solutions to your customers with T-Baito's Partner Program.</span>
							<a style="color:#fff" href="#" title="">HOW DOES IT WORK</a>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</section>
	<section>
		<div class="block double-gap-top double-gap-bottom">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="heading">
							<h2>BUILD A BETTER BUSINESS WITH T-Baito</h2>
							<span>Help your SMB clients transform their business with applicant tracking  </br> system software designed especially for them.
							</span>
						</div><!-- Heading -->
						<div class="how-to-sec style2">
							<div class="how-to">
								<span class="how-icon"><i class="la la-user"></i></span>
								<h3>Expand your services</h3>
								<p>Extend your client reach with access to groundbreaking ATS software</p>
							</div>
							<div class="how-to">
								<span class="how-icon"><i class="la la-file-archive-o"></i></span>
								<h3>Earn recurring revenue</h3>
								<p>Earn money for every paying subscriber you refer</p>
							</div>
							<div class="how-to">
								<span class="how-icon"><i class="la la-list"></i></span>
								<h3>Empower your clients</h3>
								<p>Make hiring more efﬁcient for clients with T-baito's ATS software</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block double-gap-top double-gap-bottom">
			<div data-velocity="-.1" style="background: transparent url(../images/resource/parallax1.jpg) repeat scroll 50% -54.89px;" class="parallax scrolly-invisible layer color red"></div>
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<div class="aeq text-left">
							<h5>DEEPEN YOUR RELATIONSHIPS</h5>
							<h2>Stay ahead of the curve and boost your practice</h2>
							<span>We designed the T-Baito Partner Program to help you grow and build rewarding relationships with your clients.
							</span>
						</div>
						<div class="row aeqq">
							<div class="col-lg-6 text-left">
								<h5>Marketing support to help you grow</h5>
								<span>Stay ahead of the competition by learning new market trends and the latest industry news.</span>
							</div>
							<div class="col-lg-6 text-left">
								<h5>Marketing support to help you grow</h5>
								<span>Stay ahead of the competition by learning new market trends and the latest industry news.</span>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block double-gap-top double-gap-bottom">
			<div data-velocity="-.1" style="background: transparent url(../images/resource/parallax2.jpg) repeat scroll 50% -54.89px;" class="parallax scrolly-invisible layer color red"></div>
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						
					</div>
					<div class="col-lg-6">
						<div class="aeq text-left">
							<h5>PARTNER SUPPORT & RESOURCES</h5>
							<h2>Software adn support for your growth</h2>
							<span>Enjoy free access to T-Baito ATS software, dedicated support, tools, training and access to other great resources.
							</span>
						</div>
						<div class="row aeqq">
							<div class="col-lg-6 text-left">
								<h5>Complete Autonomy</h5>
								<span>Participate as little or as much as you want, the program is completely autonomous.
								</span>
							</div>
							<div class="col-lg-6 text-left">
								<h5>Marketing & More</h5>
								<span>Get customizable assets to boost your brand with new and existing clients.</span>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block double-gap-top double-gap-bottom">
			<div data-velocity="-.1" style="background: transparent url(../images/resource/mslider2.jpg) repeat scroll 50% -54.89px;" class="parallax scrolly-invisible layer color red"></div>
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<div class="aeq text-left">
							<h5>NO PROGRAM FEES</h5>
							<h2>Monitoring</h2>
							<span>With no program fees or minimum lead requirements, becoming a T-baito ATS solutions partner is the best way to get a steady stream of revenue.
							</span>
						</div>
						<div class="row aeqq">
							<div class="col-lg-6 text-left">
								<h5>Sell More, Gain More</h5>
								<span>You will get rewarded for your participation. The more you sell, the more rewards you will receive for your efforts.</span>
							</div>
							<div class="col-lg-6 text-left">
								<h5>Level-up your Status</h5>
								<span>Take advantage of an accelerated selling structure and multi-tiered partnership level of: silver, gold and platinum.</span>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="heading">
							<h5>HOW THE T-BAITO PARTNER PROGRAM WORKS</h5>
							<h2>Partner Levels and Revenue Share Breakdown</h2>
							<span>Earn a percentage of every dollar your clients spend with T-BAITO<br>recruits. It's a pretty awesome stats area!</span>
						</div><!-- Heading -->
						<div class="stats-sec style2">
							<div class="row">
								<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
									<div class="stats">
										<div class="pl silver">Silver</div>
										<span>15%</span>
									</div>
									<div class="qqss">
										<h5>REVENUE SHARE</h5>
										<h3> Entry Level ¥0k</h3>
										<span>Annual bookings requirement</span>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
									<div class="stats">
										<div class="pl gold">Gold</div>
										<span>20%</span>
									</div>
									<div class="qqss">
										<h5>REVENUE SHARE</h5>
										<h3> Entry Level ¥0k</h3>
										<span>Annual bookings requirement</span>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
									<div class="stats">
										<div class="pl platinum"> PLATINIUM</div>
										<span>25%</span>
									</div>
									<div class="qqss">
										<h5>REVENUE SHARE</h5>
										<h3> Entry Level ¥0k</h3>
										<span>Annual bookings requirement</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php include ('footer.php'); ?>
</div>
<?php include ('foot.php'); ?>
</body>
</html>
