<!DOCTYPE html>
<html>
<?php include ('head.php'); ?>
<body>
<div class="theme-layout" id="scrollup">
	<?php include ('responsive-header.php'); ?>
	<?php include ('header.php'); ?>
	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: transparent url(../images/resource/mslider3.jpg) repeat scroll 50% -41.3px;" class="parallax scrolly-invisible no-parallax"></div>
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header"><h3>TBaito Company Register</h3></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block remove-bottom">
			<div class="container">
				<div class="row mb50">
					<div class="col-lg-4">
						<div class="about-us"><h3>Promote your your business!</h3></div>
						<p>We help you achieve business goals with engineering and consulting services which are designed especially for you!.</p>
						<br>
						<br>
						<br>
						<p>Don't hesitate to get in touch with your expert team to learn more about how you can leverage our industries and innovative solutions to maximize our proﬁts</p>
						<div class="profile-form-edit" style="margin-top: 30px">
				 			<form>
				 				<div class="row">
				 					<div class="col-lg-12">
				 						<div class="pf-field">
				 							<input type="text" placeholder="Your inquiry" />
				 						</div>
				 					</div>
				 					<div class="col-lg-12">
				 						<div class="pf-field">
				 							<textarea placeholder="Type your message here..."></textarea>
				 						</div>
				 					</div>
				 					
				 				</div>
				 			</form>
				 		</div>
					</div>
					<div class="col-lg-8">
						<h4>User information</h4>
						<div class="account-popup-area signup-popup-box static">
							<div class="account-popup delete">
								<form style="margin:0">
									<div class="row">
										<div class="col-lg-6">
											<div class="cfield">
												<input placeholder="First name" type="text">
												<i class="la la-user"></i>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="cfield">
												<input placeholder="E-mail" type="text">
												<i class="la la-envelope-o"></i>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="cfield">
												<input placeholder="Password" type="password">
												<i class="la la-key"></i>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="cfield">
												<input placeholder="Conﬁrm Password" type="password">
												<i class="la la-key"></i>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
						<h4>Company information</h4>
						<div class="account-popup-area signup-popup-box static">
							<div class="account-popup delete">
								<form style="margin:0">
									<div class="row">
										<div class="col-lg-6">
											<div class="cfield">
												<input placeholder="Company name" type="text">
												<i class="la la-user"></i>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="cfield">
												<input placeholder="Company type" type="text">
												<i class="la la-user-plus"></i>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="cfield">
												<input placeholder="Telephone number" type="password">
												<i class="la la-tty"></i>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="cfield">
												<input placeholder="Phone number" type="password">
												<i class="la la-phone-square"></i>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="cfield">
												<input placeholder="Company web" type="text">
												<i class="la la-internet-explorer"></i>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="cfield">
												<input placeholder="Company E-mail" type="text">
												<i class="la la-at"></i>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
						<h4>How can T-baito help you</h4>
						<div class="account-popup-area signup-popup-box static">
							<div class="account-popup delete">
								<form style="margin:0">
									<div class="row">
										<div class="col-lg-6">
											<div class="pf-field">
												<select data-placeholder="Select question" class="chosen"></select>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="pf-field">
												<select data-placeholder="Select question" class="chosen"></select>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
						<div class="specialism_widget">
			 				<div class="simple-checkbox">
								<p>
									<input name="spealism" id="as" type="checkbox">
									<label for="as">I agree to the Term of use & 
										<a style="color:#1bbc9b;text-decoration: underline;"href="../staff/policy.php">Privacy policy</a>
									</label>
								</p>
							</div>
			 			</div>
			 			<button class="qow" type="submit">Submit</button>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php include ('footer.php'); ?>
</div>
<?php include ('foot.php'); ?>
</body>
</html>