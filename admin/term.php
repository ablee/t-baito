<!DOCTYPE html>
<html>
<?php include ('head.php'); ?>
<body>
<div class="theme-layout" id="scrollup">
	<?php include ('responsive-header.php'); ?>
	<?php include ('header.php'); ?>
	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: transparent url(images/resource/mslider3.jpg) repeat scroll 50% -41.3px;" class="parallax scrolly-invisible no-parallax"></div>
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="ab inner-header"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block">
			<div class="container">
				 <div class="row">
				 	<div class="col-lg-12">
				 		<div class="about-us">
				 			<div class="row">
				 				<div class="col-lg-12"><h3>T-Baito Terms of Service </h3></div>
				 				<div class="col-lg-12">
				 					<p>These terms and conditions, together with the T-Baito Privacy Policy and the T-Baito, which is incorporated herein by this reference, (together, these “Terms” or this “Agreement”), are a binding legal contract between Tab Solution Inc. (d/b/a), its afﬁliates, licensors, and subsidiaries (collectively, “T-Baito”), and the individual or legal entity (including employees and agents who access or use Services on behalf of the legal entity (“Authorized Users”)) who directly or indirectly accesses, uses, or purchases the Services (as deﬁned herein) (collectively, “You” or “Your”).
 									</p>
				 				</div>
				 				<div class="col-lg-12">
				 					<p>By agreeing to these Terms, or by accessing, using or purchasing the Services, You represent that You have the authority to enter into and agree to these Terms. If You do so on behalf of a legal entity, You represent and warrant that You have the authority to accept these Terms on behalf of that legal entity. Your continued access to, use or purchase of, the Services following modiﬁcation to the Services or these Terms constitutes Your agreement to be bound by these Terms, as modiﬁed. If You do not agree to be bound by these Terms, You may not access or use the Services.</p>
				 				</div>
				 			</div>
				 		</div>
				 	</div>
				 </div>
			</div>
		</div>
	</section>
	<?php include ('footer.php'); ?>
</div>
<?php include ('foot.php'); ?>
</body>
</html>