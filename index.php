<!DOCTYPE html>
<html>
<?php include ('head.php'); ?>
<body>
<div class="theme-layout" id="scrollup">
	<?php include ('responsive-header.php'); ?>
	<?php include ('header.php'); ?>
	<section>
		<div class="block no-padding">
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="main-featured-sec">
							<ul class="main-slider-sec style2 text-arrows">
								<li class="slideHome"><img src="images/resource/mslider3.jpg" alt="" /></li>
								<li class="slideHome"><img src="images/resource/mslider2.jpg" alt="" /></li>
								<li class="slideHome"><img src="images/resource/mslider1.jpg" alt="" /></li>
							</ul>
							<div class="job-search-sec b">
								<div class="job-search">
									<h1>Explore Thousands of job Just a Click Away....</h3>
									<form method="get" action="staff/category.php">
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
												<div class="job-field">
													<input type="text" placeholder="Keyword, Title, Skills..." />
													<i class="la la-search"></i> 
												</div>
											</div>
											<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
												<div class="job-field">
													<input type="text" placeholder="Location..." />
													<i class="la la-map-marker"></i>
												</div>
											</div>
											<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
												<button type="submit">FIND JOB</button>
											</div>
										</div>
									</form>
									<div class="row">
										<div class="all-in col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center">
											<div class="or-browser">
												<b>job type</b>
												<a href="#" title="">Daily</a>
												<a href="#" title="">Part time</a>
												<a href="#" title="">Full time</a>
											</div>
										</div>
										<div class="all-in col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center">
											<div class="or-browser">
												<b>Payment</b>
												<a href="#" title="">Hourly</a>
												<a href="#" title="">Daily</a>
												<a href="#" title="">Weakly</a>
											</div>
										</div>
										<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center">
											<div class="or-browser">
												<b>Shift</b>
												<a href="#" title="">Morning</a>
												<a href="#" title="">Afternoon</a>
												<a href="#" title="">Evening</a>
											</div>
										</div>
										<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center">
											<div class="or-browser">
												<b>Date</b>
												<a href="#" title="">Today</a>
												<a href="#" title="">Yesterday</a>
												<a href="#" title="">Last week</a>
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<div class="download-sec">
												<div class="download-text">
													<ul>
														<li>
															<a href="#" title="">
																<i class="la la-apple"></i>
																<span>App Store</span>
																<p>Available now on the</p>
															</a>
														</li>
														<li>
															<a href="#" title="">
																<i class="la la-android"></i>
																<span>Google Play</span>
																<p>Get in on</p>
															</a>
														</li>
														<li class="">
															<a class="signup-popup" title="">
																<i class="la la-search-plus"></i>
																<span>Advenced search</span>
																<p>More in</p>
															</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="scroll-to style2">
							<a href="#scroll-here" title=""><i class="la la-arrow-down"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="scroll-here">
		<div class="block">
			<div class="container">
				 <div class="row">
				 	<div class="col-lg-12">
				 		<div class="filterbar">
				 			<h5>Latest jobs</h5>
				 		</div>
				 		<div class="job-grid-sec">
							<div class="row" id="masonry">
								<div class="col-lg-8 column">
									<div class="emply-list-sec">
							 			<div class="row" id="masonry">
							 				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="job-grid border">
													<div class="job-title-sec">
														<div class="c-logo" style="background-image:url(images/resource/b1.jpg)">
														</div>
														<div class="type">
															<a href="staff/details.php" title=""><i class="la la-briefcase"></i>Daily job</a>
															<a href="staff/details.php">
																<div class="rating">
																	<i class="la la-star"></i>
																	<i class="la la-star"></i>
																	<i class="la la-star"></i>
																	<i class="la la-star"></i>
																	<i class="la la-star-half-o"></i>
																</div>
															</a>
														</div>
														<h3><a href="staff/details.php" title="">ファミリーマート　新宿京王モール店のアルバイ  新宿京王モール店のアルバイ</a></h3>
														<span class="fav-job"><i class="la la-heart-o"></i></span>
														<h6><i class="la la-map-marker"></i>Location</h6>
														<h6><i class="la la-clock-o"></i>Shif date</h6>
														<h6><i class="la la-money"></i>Salary</h6>
													</div>
													<span class="job-lctn">Company name<br>
														<time>22 seconds ago</time>
													</span>
													<a href="staff/details.php" title="">APPLY<i class="la la-plus"></i></a>
												</div><!-- JOB Grid -->
											</div>
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="job-grid border">
													<div class="job-title-sec">
														<div class="c-logo" style="background-image:url(images/resource/b1.jpg)">
														</div>
														<div class="type">
															<a href="staff/details.php" title=""><i class="la la-briefcase"></i>Daily job</a>
															<a href="staff/details.php">
																<div class="rating">
																	<i class="la la-star"></i>
																	<i class="la la-star"></i>
																	<i class="la la-star"></i>
																	<i class="la la-star"></i>
																	<i class="la la-star-half-o"></i>
																</div>
															</a>
														</div>
														<h3><a href="staff/details.php" title="">ファミリーマート　新宿京王モール店のアルバイ  新宿京王モール店のアルバイ</a></h3>
														<span class="fav-job"><i class="la la-heart-o"></i></span>
														<h6><i class="la la-map-marker"></i>Location</h6>
														<h6><i class="la la-clock-o"></i>Shif date</h6>
														<h6><i class="la la-money"></i>Salary</h6>
													</div>
													<span class="job-lctn">Company name<br>
														<time>22 seconds ago</time>
													</span>
													<a href="staff/details.php" title="">APPLY<i class="la la-plus"></i></a>
												</div><!-- JOB Grid -->
											</div>
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="job-grid border">
													<div class="job-title-sec">
														<div class="c-logo" style="background-image:url(images/resource/b1.jpg)">
														</div>
														<div class="type">
															<a href="staff/details.php" title=""><i class="la la-briefcase"></i>Daily job</a>
															<a href="staff/details.php">
																<div class="rating">
																	<i class="la la-star"></i>
																	<i class="la la-star"></i>
																	<i class="la la-star"></i>
																	<i class="la la-star"></i>
																	<i class="la la-star-half-o"></i>
																</div>
															</a>
														</div>
														<h3><a href="staff/details.php" title="">ファミリーマート　新宿京王モール店のアルバイ  新宿京王モール店のアルバイ</a></h3>
														<span class="fav-job"><i class="la la-heart-o"></i></span>
														<h6><i class="la la-map-marker"></i>Location</h6>
														<h6><i class="la la-clock-o"></i>Shif date</h6>
														<h6><i class="la la-money"></i>Salary</h6>
													</div>
													<span class="job-lctn">Company name<br>
														<time>22 seconds ago</time>
													</span>
													<a href="staff/details.php" title="">APPLY<i class="la la-plus"></i></a>
												</div><!-- JOB Grid -->
											</div>
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="job-grid border">
													<div class="job-title-sec">
														<div class="c-logo" style="background-image:url(images/resource/b1.jpg)">
														</div>
														<div class="type">
															<a href="staff/details.php" title=""><i class="la la-briefcase"></i>Daily job</a>
															<a href="staff/details.php">
																<div class="rating">
																	<i class="la la-star"></i>
																	<i class="la la-star"></i>
																	<i class="la la-star"></i>
																	<i class="la la-star"></i>
																	<i class="la la-star-half-o"></i>
																</div>
															</a>
														</div>
														<h3><a href="staff/details.php" title="">ファミリーマート　新宿京王モール店のアルバイ  新宿京王モール店のアルバイ</a></h3>
														<span class="fav-job"><i class="la la-heart-o"></i></span>
														<h6><i class="la la-map-marker"></i>Location</h6>
														<h6><i class="la la-clock-o"></i>Shif date</h6>
														<h6><i class="la la-money"></i>Salary</h6>
													</div>
													<span class="job-lctn">Company name<br>
														<time>22 seconds ago</time>
													</span>
													<a href="staff/details.php" title="">APPLY<i class="la la-plus"></i></a>
												</div><!-- JOB Grid -->
											</div>
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="job-grid border">
													<div class="job-title-sec">
														<div class="c-logo" style="background-image:url(images/resource/b1.jpg)">
														</div>
														<div class="type">
															<a href="staff/details.php" title=""><i class="la la-briefcase"></i>Daily job</a>
															<a href="staff/details.php">
																<div class="rating">
																	<i class="la la-star"></i>
																	<i class="la la-star"></i>
																	<i class="la la-star"></i>
																	<i class="la la-star"></i>
																	<i class="la la-star-half-o"></i>
																</div>
															</a>
														</div>
														<h3><a href="staff/details.php" title="">ファミリーマート　新宿京王モール店のアルバイ  新宿京王モール店のアルバイ</a></h3>
														<span class="fav-job"><i class="la la-heart-o"></i></span>
														<h6><i class="la la-map-marker"></i>Location</h6>
														<h6><i class="la la-clock-o"></i>Shif date</h6>
														<h6><i class="la la-money"></i>Salary</h6>
													</div>
													<span class="job-lctn">Company name<br>
														<time>22 seconds ago</time>
													</span>
													<a href="staff/details.php" title="">APPLY<i class="la la-plus"></i></a>
												</div><!-- JOB Grid -->
											</div>
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="job-grid border">
													<div class="job-title-sec">
														<div class="c-logo" style="background-image:url(images/resource/b1.jpg)">
														</div>
														<div class="type">
															<a href="staff/details.php" title=""><i class="la la-briefcase"></i>Daily job</a>
															<a href="staff/details.php">
																<div class="rating">
																	<i class="la la-star"></i>
																	<i class="la la-star"></i>
																	<i class="la la-star"></i>
																	<i class="la la-star"></i>
																	<i class="la la-star-half-o"></i>
																</div>
															</a>
														</div>
														<h3><a href="staff/details.php" title="">ファミリーマート　新宿京王モール店のアルバイ  新宿京王モール店のアルバイ</a></h3>
														<span class="fav-job"><i class="la la-heart-o"></i></span>
														<h6><i class="la la-map-marker"></i>Location</h6>
														<h6><i class="la la-clock-o"></i>Shif date</h6>
														<h6><i class="la la-money"></i>Salary</h6>
													</div>
													<span class="job-lctn">Company name<br>
														<time>22 seconds ago</time>
													</span>
													<a href="staff/details.php" title="">APPLY<i class="la la-plus"></i></a>
												</div><!-- JOB Grid -->
											</div>
										</div>
							 		</div>
								</div>
								<aside class="col-lg-4 column">
									<div class="widget">
										<h3>Need job seeking advice? <br> We are here to help !</h3>
								 		<div class="edu-history style2">
				 							<i></i>
				 							<div class="edu-hisinfo">
				 								<h3>Making successful applications</h3>
				 								<a href="#">More</a>
				 							</div>
				 						</div>
				 						<div class="edu-history style2">
				 							<i></i>
				 							<div class="edu-hisinfo">
				 								<h3>Managing the interview </h3>
				 								<a href="#">More</a>
				 							</div>
				 						</div>
				 						<div class="edu-history style2">
				 							<i></i>
				 							<div class="edu-hisinfo">
				 								<h3>Tips for next job interview</h3>
				 								<a href="#">More</a>
				 							</div>
				 						</div>
								 	</div>
									<div class="widget">
							 			<h3>Popular jobs</h3>
							 			<div class="popular">
									 		<div class="row" style="clear: both;">
									 			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
									 				<div class="c-side-img" style="background-image:url(images/resource/b1.jpg)"></div>
									 			</div>
									 			<div class="col-lg-8 col-md-8 col-sm-6 col-xs-6">
									 				<div class="c-side-jobs">
									 					<div class="title">
									 						<a href="staff/details.php">Job title</a>
									 					</div>
									 					<div class="desc">
									 						Company Name / Daily Job
									 					</div>
									 				</div>
									 			</div>
									 			<div class="col-lg-7 col-md-7 col-sm-6 col-xs-6">
									 				<div class="tag">
									 					<h6><i class="la la-map-marker"></i>Location</h6>
														<h6><i class="la la-money"></i>Salary</h6>
									 				</div>
									 			</div>
									 			<div class="col-lg-5 col-md-5 col-sm-6 col-xs-6">
									 				<div class="job-grid more-in">
									 					<a href="staff/details.php" title="">APPLY<i class="la la-plus"></i></a>
									 				</div>
									 			</div>
									 		</div>
									 	</div>
									 	<div class="popular">
									 		<div class="row" style="clear: both;">
									 			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
									 				<div class="c-side-img" style="background-image:url(images/resource/b1.jpg)"></div>
									 			</div>
									 			<div class="col-lg-8 col-md-8 col-sm-6 col-xs-6">
									 				<div class="c-side-jobs">
									 					<div class="title">
									 						<a href="staff/details.php">Job title</a>
									 					</div>
									 					<div class="desc">
									 						Company Name / Daily Job
									 					</div>
									 				</div>
									 			</div>
									 			<div class="col-lg-7 col-md-7 col-sm-6 col-xs-6">
									 				<div class="tag">
									 					<h6><i class="la la-map-marker"></i>Location</h6>
														<h6><i class="la la-money"></i>Salary</h6>
									 				</div>
									 			</div>
									 			<div class="col-lg-5 col-md-5 col-sm-6 col-xs-6">
									 				<div class="job-grid more-in">
									 					<a href="staff/details.php" title="">APPLY<i class="la la-plus"></i></a>
									 				</div>
									 			</div>
									 		</div>
									 	</div>
									 	<div class="popular">
									 		<div class="row" style="clear: both;">
									 			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
									 				<div class="c-side-img" style="background-image:url(images/resource/b1.jpg)"></div>
									 			</div>
									 			<div class="col-lg-8 col-md-8 col-sm-6 col-xs-6">
									 				<div class="c-side-jobs">
									 					<div class="title">
									 						<a href="staff/details.php">Job title</a>
									 					</div>
									 					<div class="desc">
									 						Company Name / Daily Job
									 					</div>
									 				</div>
									 			</div>
									 			<div class="col-lg-7 col-md-7 col-sm-6 col-xs-6">
									 				<div class="tag">
									 					<h6><i class="la la-map-marker"></i>Location</h6>
														<h6><i class="la la-money"></i>Salary</h6>
									 				</div>
									 			</div>
									 			<div class="col-lg-5 col-md-5 col-sm-6 col-xs-6">
									 				<div class="job-grid more-in">
									 					<a href="staff/details.php" title="">APPLY<i class="la la-plus"></i></a>
									 				</div>
									 			</div>
									 		</div>
									 	</div>
							 		</div>
							 		<div class="widget">
										<h3>Featured videos</h3>
								 		<div class="video text-center" style="background-image: url(images/resource/parallax1.jpg);">
											<a class="video-popup video-play-popup">
												<i class="la la-play"></i>
											</a>
										</div>
								 	</div>
								</aside>
							</div>
						</div>
				 	</div>
				 </div>
			</div>
		</div>
	</section>
	<section>
		<div class="block gray">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="heading">
							<h2>News and Insights</h2>
						</div><!-- Heading -->
						<div class="top-company-sec">
							<div class="row" id="companies-carousel">
								<div class="col-lg-4">
									<div class="my-blog">
										<div class="blog-thumb">
											<a href="staff/news-single.php" title=""><img src="images/resource/b1.jpg" alt=""></a>
											<div class="blog-date">
												<a href="#" title="">2018.05.28</a>
											</div>
										</div>
										<div class="blog-details">
											<h3><a href="staff/news-single.php" title="">Attract More Attention Sales And Profits</a></h3>
											<p>A job is a regular activity performed in exchange becoming an employee, volunteering, </p>
											<a href="staff/news-single.php" title="">Read More <i class="la la-long-arrow-right"></i></a>
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="my-blog">
										<div class="blog-thumb">
											<a href="staff/news-single.php" title=""><img src="images/resource/b1.jpg" alt=""></a>
											<div class="blog-date">
												<a href="#" title="">2018.05.28</a>
											</div>
										</div>
										<div class="blog-details">
											<h3><a href="staff/news-single.php" title="">Attract More Attention Sales And Profits</a></h3>
											<p>A job is a regular activity performed in exchange becoming an employee, volunteering, </p>
											<a href="staff/news-single.php" title="">Read More <i class="la la-long-arrow-right"></i></a>
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="my-blog">
										<div class="blog-thumb">
											<a href="staff/news-single.php" title=""><img src="images/resource/b1.jpg" alt=""></a>
											<div class="blog-date">
												<a href="#" title="">2018.05.28</a>
											</div>
										</div>
										<div class="blog-details">
											<h3><a href="staff/news-single.php" title="">Attract More Attention Sales And Profits</a></h3>
											<p>A job is a regular activity performed in exchange becoming an employee, volunteering, </p>
											<a href="staff/news-single.php" title="">Read More <i class="la la-long-arrow-right"></i></a>
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="my-blog">
										<div class="blog-thumb">
											<a href="staff/news-single.php" title=""><img src="images/resource/b1.jpg" alt=""></a>
											<div class="blog-date">
												<a href="#" title="">2018.05.28</a>
											</div>
										</div>
										<div class="blog-details">
											<h3><a href="staff/news-single.php" title="">Attract More Attention Sales And Profits</a></h3>
											<p>A job is a regular activity performed in exchange becoming an employee, volunteering, </p>
											<a href="staff/news-single.php" title="">Read More <i class="la la-long-arrow-right"></i></a>
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="my-blog">
										<div class="blog-thumb">
											<a href="staff/news-single.php" title=""><img src="images/resource/b1.jpg" alt=""></a>
											<div class="blog-date">
												<a href="#" title="">2018.05.28</a>
											</div>
										</div>
										<div class="blog-details">
											<h3><a href="staff/news-single.php" title="">Attract More Attention Sales And Profits</a></h3>
											<p>A job is a regular activity performed in exchange becoming an employee, volunteering, </p>
											<a href="staff/news-single.php" title="">Read More <i class="la la-long-arrow-right"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="browse-all-cat">
							<a href="#" title="" class="style1">Read more</a>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</section>
	<section>
		<div class="block">
			<div data-velocity="-.1" style="background: transparent url(&quot;images/resource/parallax1.jpg&quot;) repeat scroll 50% -19.76px;" class="parallax scrolly-invisible layer color"></div>
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="subscription-sec">
							<div class="row">
								<div class="col-lg-6">
									<h3>Subscribe to our newsletter</h3>
								</div>
								<div class="col-lg-6">
									<form>
										<input type="text" placeholder="Enter Valid Email Address" />
										<button type="submit">Sign up <i class="la la-paper-plane"></i></button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block gray">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="heading">
							<h2>Our Clients</h2>
						</div><!-- Heading -->
						<div class="team-sec">
							<div class="row" id="team-carousel">
								<div class="col-lg-3">
									<div class="top-compnay style2">
										<img src="images/resource/cc1.jpg" alt="" />
										<h3><a href="#" title="">Company name</a></h3>
									</div><!-- Top Company -->
								</div>
								<div class="col-lg-3">
									<div class="top-compnay style2">
										<img src="images/resource/cc2.jpg" alt="" />
										<h3><a href="#" title="">Company name</a></h3>
									</div><!-- Top Company -->
								</div>
								<div class="col-lg-3">
									<div class="top-compnay style2">
										<img src="images/resource/cc3.jpg" alt="" />
										<h3><a href="#" title="">Company name</a></h3>
									</div><!-- Top Company -->
								</div>
								<div class="col-lg-3">
									<div class="top-compnay style2">
										<img src="images/resource/cc4.jpg" alt="" />
										<h3><a href="#" title="">Company name</a></h3>
									</div><!-- Top Company -->
								</div>
								<div class="col-lg-3">
									<div class="top-compnay style2">
										<img src="images/resource/cc5.jpg" alt="" />
										<h3><a href="#" title="">Company name</a></h3>
									</div><!-- Top Company -->
								</div>
								<div class="col-lg-3">
									<div class="top-compnay style2">
										<img src="images/resource/cc1.jpg" alt="" />
										<h3><a href="#" title="">Company name</a></h3>
									</div><!-- Top Company -->
								</div>
								<div class="col-lg-3">
									<div class="top-compnay style2">
										<img src="images/resource/cc2.jpg" alt="" />
										<h3><a href="#" title="">Company name</a></h3>
									</div><!-- Top Company -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block">
			<div data-velocity="-.3" style="background: transparent url(images/resource/parallax6.jpg) repeat scroll 50% -39.93px;" class="parallax scrolly-invisible"></div>
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="heading">
							<h2>What members say</h2>
						</div><!-- Heading -->
						<div class="team-sec">
							<div class="row">
								<div class="col-lg-3">
									<div class="team">
										<div class="team-img"><img src="images/resource/r1.jpg" alt="" /></div>
										<div class="team-detail">
											<h3><a href="#" title="">Mr. Ms</a></h3>
											<p>The Random Name Generator is a simple fiction writing tool…</p>
										</div>
									</div><!-- Team -->
								</div>
								<div class="col-lg-3">
									<div class="team">
										<div class="team-img"><img src="images/resource/r2.jpg" alt="" /></div>
										<div class="team-detail">
											<h3><a href="#" title="">Mr. Ms</a></h3>
											<p>The Random Name Generator is a simple fiction writing tool…</p>
										</div>
									</div><!-- Team -->
								</div>
								<div class="col-lg-3">
									<div class="team">
										<div class="team-img"><img src="images/resource/r1.jpg" alt="" /></div>
										<div class="team-detail">
											<h3><a href="#" title="">Mr. Ms</a></h3>
											<p>The Random Name Generator is a simple fiction writing tool…</p>
										</div>
									</div><!-- Team -->
								</div>
								<div class="col-lg-3">
									<div class="team">
										<div class="team-img"><img src="images/resource/r2.jpg" alt="" /></div>
										<div class="team-detail">
											<h3><a href="#" title="">Mr. Ms</a></h3>
											<p>The Random Name Generator is a simple fiction writing tool…</p>
										</div>
									</div><!-- Team -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</section>
	<section>
		<div class="block">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="heading">
							<h2>Feachures</h2>
						</div><!-- Heading -->
						<div class="cat-sec">
							<div class="row no-gape">
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									<div class="p-category green">
										<a href="#" title="">
											<img src="images/resource/icon-1.png" />
											<span>Millions of jobs</span>
											<p>Quickly and easily find open positions tailored for you and fitting your skills and experience. We provide daily job to maximize your spare­time and fulfill your every single day</p>
										</a>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									<div class="p-category green">
										<a href="#" title="">
											<img src="images/resource/icon-2.png" />
											<span>Get hired immediately</span>
											<p>Create an account and upload your resume  to be found by the recruiters who search T­Baito's  databases every day. O </p>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="cat-sec">
							<div class="row no-gape">
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									<div class="p-category green">
										<a href="#" title="">
											<img style="width:55px" src="images/resource/icon-4.png" />
											<span>Smart matches</span>
											<p>We’ll then contact you if your qualifications meet the requirements of an open position or match what our clients typically look for.
											</p>
										</a>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									<div class="p-category green">
										<a href="#" title="">
											<img style="width:55px" src="images/resource/icon-5.png" />
											<span>HR systems</span>
											<p>Manage your candidates in hand! Connect Job seekers & Headhunters! It’s all coming together!!!  Explore how we make it!</p>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php include ('footer.php'); ?>
</div>
<?php include ('foot.php'); ?>
</body>
</html>

