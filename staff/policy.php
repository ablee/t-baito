<!DOCTYPE html>
<html>
<?php include ('head.php'); ?>
<body>
<div class="theme-layout" id="scrollup">
	<?php include ('responsive-header.php'); ?>
	<?php include ('header.php'); ?>
	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: transparent url(../images/resource/mslider3.jpg) repeat scroll 50% -41.3px;" class="parallax scrolly-invisible no-parallax"></div>
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="ab inner-header"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block">
			<div class="container">
				 <div class="row">
				 	<div class="col-lg-12">
				 		<div class="about-us">
				 			<div class="row">
				 				<div class="col-lg-12"><h3>Privacy policy</h3></div>
				 				<div class="col-lg-12 faq-box job-details">
				 					<h2>1. Deﬁnitions andParties</h2>
 									<p>The treatment of the information collected through the use of T-baito.com and through other Sites, herein collectively referred to as the Service, is governed by this Privacy Policy.</p>
 									<p>This is a legal document between GPlusMedia Inc., theadministrator of this Service, and you, the user of this Service.</p>
 									<p>Tab-solutions LLC is committed to protecting your privacy.</p>
 									<p>We do not sell, share or lease your information in ways other thanstated in this Privacy Policy.</p>
 									<p>By using this Service, you show your binding acceptance to thepresent Privacy Policy.</p>
 									<p>You are assumed to use this Service at your own discretion andresponsibility. You must be a legal adult or be supervised by a parent orguardian to use this Service.</p>
 									<p>In this document you will ﬁnd information about:</p>
 									<ul>
 										<li>Personal information and data collection process</li>
 										<li>Use of information</li>
 										<li>Disclosure of personal information</li>
 										<li>Personal information protection measures</li>
 										<li>Your ability to access, modify and remove your personal information</li>
 									</ul>
 									<p>Should you have any question, please feel free to Contact Us.</p>
 									<h2>2. Personal Information</h2>
 									<p>This Service’s public content is visible to everybody. However, in order to use certain features, you have to register by providing the following personal information:</p>
 									<ul>
 										<li>Email address</li>
 										<li>Password</li>
 									</ul>
 									<p>In order to build resumes to apply to jobs, you have to providethe following personal information:</p>
 									<ul>
 										<li>Name</li>
 										<li>Birth date</li>
 										<li>Gender</li>
 										<li>Nationality</li>
 										<li>Country, state and city of residence</li>
 										<li>Japan visa status</li>
 									</ul>
 									<p>You may choose to provide these additional pieces of informationto further your chance of getting a job.</p>
 									<ul>
 										<li>Phone number</li>
 										<li>Closest train station</li>
 										<li>Education</li>
 										<li>Work history</li>
 									</ul>
 									<p>When posting a resume, you can control the level of visibility bychoosing to:</p>
 									<ul>
 										<li>. Keep it private: Employers cannot look at this resume unless you apply to one of their job postings. This is the default.</li>
 										<li>Make it searchable by employers: Employers will be able to look at an abridged version of your resume and send you an invitation via this Service. Your personal information will only become visible once you accept the invitation or apply to one of their job postings.</li>
 									</ul>
 									<p>You can change the level of visibility of your resume at any time.</p>
 									<h2>3. Collection of Personal Information</h2>
 									<p>If you choose to use one of the services listed below to registerwe will collect certain personal information from that service in order tostart your resume.</p>
 									<ul>
 										<li>Facebook via Facebook Connect</li>
 										<li>LinkedIn via LinkedIn Connections API</li>
 										<li>Google Email</li>
 										<li>Twitter</li>
 									</ul>
 									<p>We also collect information from your web trafﬁc in order to maintain the quality of the Service and to facilitate your navigation. The collection methods are:</p>
 									<ul>
 										<li>Log ﬁles</li>
 										<li>Cookies</li>
 										<li>Web beacons</li>
 										<li>Google Analytics</li>
 										<li>Facebook Pixel</li>
 									</ul>
 									<h2>4. Use of PersonalInformation</h2>
 									<p>Your personal information is used in the following cases:</p>
 									<ul>
 										<li>Service communication: email newsletters and notiﬁcations</li>
 										<li>Interaction with employers: applications, resume bank invitations, messages</li>
 										<li>Anonymised data for statistical research</li>
 									</ul>
 									<h2>5. Disclosure of Personal Information</h2>
 									<p>When you apply to a job or accept an invitation from an employeryour resume is shared with them.</p>
 									<p>You can view your resumes, application history and interactionwith employers from your account. For other pieces of personal information wemay hold about you please contact us.</p>
 									<p>Your personal information is never disclosed without your consentto third parties except in the following cases:</p>
 									<ul>
 										<li>If ordered by governmental authorities</li>
 										<li>If it is required by law</li>
 									</ul>
 									<h2>6. Modiﬁcation ofPersonal Information</h2>
 									<p>You can modify your resumes and proﬁle from your account at anytime. Your previously submitted applications cannot be modiﬁed. However youcan withdraw them.</p>
 									<p>Please contact us through our Contact Form if you need any assistance.</p>
 									<h2>7. Removal of PersonalInformation</h2>
 									<p>You can delete your resumes, however previously submittedapplications will still be visible to employers unless you withdraw them.</p>
 									<p>You can close your account, after which you have a six month graceperiod to reactivate it before it is permanently removed. After closing youraccount your applications are no longer visible to employers.</p>
 									<p>Please contact us through our Contact Form if you need any assistance.</p>
 									<h2>8. Links</h2>
 									<p>This Service contains links to external sites administered by third parties. Tab-Solutions LLC. shall not be held responsible for any PrivacyPolicy issues related to such external sites.</p>
 									<p>When redirected to an external site, you should check theapplicable Privacy Policy.
 									</p>
 									<h2>9. InformationProtection Measures</h2>
 									<p>Tab-Solutions LLC. 's internal privacy and security policies are reviewed on a regular basis. Our staff receive training on information protection measures accordingly.</p>
 									<p>We use industry standard procedures and technologies to keep yourpersonal information safe.</p>
 									<h2>10. Data Protection and PrivacyLaws</h2>
 									<p>We comply with all relevant data protection and privacy laws such as the General Data Protection Regulation (GDPR). Please contact us through our Contact Form if you have any inquiries.</p>
 									<h2>11. Modiﬁcation of Privacy Policy</h2>
 									<p>GPlusMedia Inc. reserves the right to modify this Privacy Policy without prior notice. Y our continued use of the Service after the revised Privacy Policy has become effective indicates that you have read, understood and agreed to the current version of the Privacy Policy</p>
				 				</div>
				 			</div>
				 		</div>
				 	</div>
				 </div>
			</div>
		</div>
	</section>
	<?php include ('footer.php'); ?>
</div>
<?php include ('foot.php'); ?>
</body>
</html>