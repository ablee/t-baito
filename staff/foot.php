<div class="account-popup-area video-popup-box">
	<div class="account-popup video-na">
		<span class="close-popup"><i class="la la-close"></i></span>
		<h3>Why do our customers love to recruit with T-Baito?</h3>
		<iframe style="margin-top:25px" width="100%" height="400px" src="https://www.youtube.com/embed/RhprOoSs9sk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
	</div>
</div>

<div class="account-popup-area signup-popup-box">
	<div class="account-popup advanceds">
		<span class="close-popup"><i class="la la-close"></i></span>
		<h3>Advanced search</h3>
		<form>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<span class="pf-title">Job type</span>
					<div class="pf-field bq">
						<div class="simple-checkbox blood">
							<p><input name="spealism" id="t1" type="checkbox"><label for="t1">Full time</label></p>
							<p><input name="spealism" id="t2" type="checkbox"><label for="t2">Parttime</label></p>
							<p><input name="spealism" id="t3" type="checkbox"><label for="t3">Contract</label></p>
							<p><input name="spealism" id="t4" type="checkbox"><label for="t4">Internship</label></p>
							<p><input name="spealism" id="t5" type="checkbox"><label for="t5">Temp</label></p>
							<p><input name="spealism" id="t6" type="checkbox"><label for="t6">Other</label></p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
					<span class="pf-title">Category</span>
					<div class="pf-field">
						<select data-placeholder="Building & Construction" class="chosen">
							<option>Building & Construction</option>
							<option>Financial services</option>
							<option>Transportation</option>
							<option>Engineering</option>
							<option>Manufacturing</option>
						</select>
					</div>
				</div>
				<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
					<span class="pf-title">Keyword</span>
					<div class="pf-field">
						<input type="text" placeholder="Keyword, Title, Skills..." />
						<i class="la la-search"></i> 
					</div>
				</div>
				<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
					<span class="pf-title">Location</span>
					<div class="pf-field">
						<input type="text" placeholder="Location..." />
						<i class="la la-map-marker"></i>
					</div>
				</div>
				
				<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
					<span class="pf-title">Salary</span>
					<div class="pf-field ma">
						<select data-placeholder="Hourly" class="chosen">
							<option>Hourly</option>
							<option>Daily</option>
							<option>Weekly</option>
							<option>Monthly</option>
						</select>
					</div>
					<div class="pf-field mz">
						<select data-placeholder="Any" class="chosen">
							<option>Any</option>
							<option>100000￥­140000￥</option>
							<option>150000￥­190000￥</option>
							<option>200000￥­240000￥</option>
							<option>250000￥­290000￥</option>
							<option>300000￥+</option>
						</select>
					</div>
				</div>
				<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
					<span class="pf-title">Language level</span>
					<div class="pf-field bo">
						<select data-placeholder="Elementary" class="chosen">
							<option>Elementary</option>
							<option>Basic</option>
							<option>Basic</option>
							<option>Advanced</option>
							<option>Floant</option>
							<option>Native</option>
						</select>
					</div>
				</div>
				<div class="col-lg-6">
					<button class="result">Result (10)</button>
				</div>
				<div class="col-lg-6">
					<button type="submit">Search for Jobs</button>
				</div>
			</div>				
		</form>
	</div>
</div>
<script src="../js/jquery.min.js" type="text/javascript"></script>
<script src="../js/modernizr.js" type="text/javascript"></script>
<script src="../js/script.js" type="text/javascript"></script>
<script src="../js/wow.min.js" type="text/javascript"></script>
<script src="../js/slick.min.js" type="text/javascript"></script>
<script src="../js/parallax.js" type="text/javascript"></script>
<script src="../js/select-chosen.js" type="text/javascript"></script>
<script src="../js/jquery.scrollbar.min.js" type="text/javascript"></script>