<!DOCTYPE html>
<html>
<?php include ('head.php'); ?>
<body>
<div class="theme-layout" id="scrollup">
	<?php include ('responsive-header.php'); ?>
	<?php include ('header.php'); ?>
	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: transparent url(../images/resource/mslider3.jpg) repeat scroll 50% -41.3px;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">
							<h3>Explore Thousands of job Just a Click Away....</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block">
			<div class="container">
				<div class="row">
				 	<div class="col-lg-12 column">
				 		<div class="job-single-sec style3">
				 			<div class="job-head-wide">
				 				<div class="row">
				 					<div class="col-lg-8">
				 						<div class="job-single-head3">
							 				<div class="job-thumb"> 
							 					<img src="../images/resource/a.png" alt="" />
							 				</div>
							 				<div class="job-single-info3">
							 					<h3>Company name</h3>
							 					<ul class="tags-jobs">
								 					<li><i class="la la-file-text"></i> Application</li>
								 					<li><i class="la la-calendar-o"></i> Post Date</li>
								 					<li><i class="la la-eye"></i> Views</li>
								 				</ul>
							 				</div>
							 				<div class="share-bar">
							 					<b>Daily job</b>
								 				<span>Share</span><a href="#" title="" class="share-fb"><i class="fa fa-facebook"></i></a><a href="#" title="" class="share-twitter"><i class="fa fa-twitter"></i></a>
								 				<a href="#" title="" class="share-google"><i class="la la-google"></i></a>
								 			</div>
							 			</div><!-- Job Head -->
				 					</div>
				 					<div class="col-lg-4">
				 						<a class="apply-thisjob" href="#" title="">
				 							<i class="la la-paper-plane"></i>Apply for job
				 						</a>
				 					</div>
				 				</div>
				 			</div>
				 			<div class="job-wide-devider">
							 	<div class="row">
							 		<div class="col-lg-8 column">		
							 			<div class="job-details ">
							 				<div class="job-overview beis">
									 			<h3>Job Overview</h3>
									 			<ul>
									 				<li><i class="la la-money"></i><h3>Offerd Salary</h3><span>¥5000-¥10000
									 				</span></li>
									 				<li><i class="la la-bars"></i><h3>Category</h3><span>IT technology</span></li>
									 				<li><i class="la la-clock-o"></i><h3>Shift</h3><span>5 times a week</span></li>
									 				<li><i class="la la-puzzle-piece"></i><h3>Commuting cost</h3><span>Partially paid</span></li>
									 			</ul>
									 		</div><!-- Job Overview -->
							 				<h3>Job Description</h3>
							 				<p>Company is a 2016 Iowa City-born start-up that develops consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.</p>
							 				<p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod dui, eu pulvinar nunc sapien</p>
							 				<h3>Required Knowledge, Skills, and Abilities</h3>
							 				<ul>
							 					<li>Ability to write code – HTML & CSS (SCSS flavor of SASS preferred when writing CSS)</li>
							 					<li>Proficient in Photoshop, Illustrator, bonus points for familiarity with Sketch (Sketch is our preferred concepting)</li>
							 					<li>Cross-browser and platform testing as standard practice</li>
							 					<li>Experience using Invision a plus</li>
							 					<li>Experience in video production a plus or, at a minimum, a willingness to learn</li>
							 				</ul>
							 				<h3>Education + Experience</h3>
							 				<ul>
							 					<li>Advanced degree or equivalent experience in graphic and web design</li>
							 					<li>3 or more years of professional design experience</li>
							 					<li>Direct response email experience</li>
							 					<li>Ecommerce website design experience</li>
							 					<li>Familiarity with mobile and web apps preferred</li>
							 					<li>Excellent communication skills, most notably a demonstrated ability to solicit and address creative and design feedback</li>
							 					<li>Must be able to work under pressure and meet deadlines while maintaining a positive attitude and providing exemplary customer service</li>
							 					<li>Ability to work independently and to carry out assignments to completion within parameters of instructions given, prescribed routines, and standard accepted practices</li>
							 				</ul>
							 			</div>
								 		<div class="recent-jobs">
							 				<div class="emply-list">
									 			<div class="row a">
									 				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									 					<div class="title">
									 						<a href="staff/details.php">job title</a>
									 					</div>
									 				</div>
									 				<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
									 					<div class="thumb" style="background-image:url(../images/resource/b1.jpg)"></div>
									 				</div>
									 				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
									 					<div class="content">
									 						<li><i class="la la-money"></i>Salary</li>
									 						<li><i class="la la-map-marker"></i>Location</li>
									 						<li><i class="la la-clock-o"></i>Shift</li>
									 					</div>
									 				</div>
									 				<div class="col-lg-5 col-md-4 col-sm-12 col-xs-12">
									 					<div class="base">
									 						Company Name / Daily Job
									 					</div>
									 				</div>
									 				<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
									 					<div class="job-but">
									 						<a href="staff/details.php"><span class="apply">Apply</span></a>
															<i><a href="staff/details.php">End date</a></i>
														</div>
									 				</div>
									 			</div>
									 		</div>
							 			</div>
							 			<div class="emply-list">
								 			<div class="row a">
								 				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								 					<div class="title">
								 						<a href="staff/details.php">job title</a>
								 					</div>
								 				</div>
								 				<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
								 					<div class="thumb" style="background-image:url(../images/resource/b1.jpg)"></div>
								 				</div>
								 				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
								 					<div class="content">
								 						<li><i class="la la-money"></i>Salary</li>
								 						<li><i class="la la-map-marker"></i>Location</li>
								 						<li><i class="la la-clock-o"></i>Shift</li>
								 					</div>
								 				</div>
								 				<div class="col-lg-5 col-md-4 col-sm-12 col-xs-12">
								 					<div class="base">
								 						Company Name / Daily Job
								 					</div>
								 				</div>
								 				<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
								 					<div class="job-but">
								 						<a href="staff/details.php"><span class="apply">Apply</span></a>
														<i><a href="staff/details.php">End date</a></i>
													</div>
								 				</div>
								 			</div>
								 		</div>
							 		</div>
							 		<div class="col-lg-4 column">
							 			<div class="job-location job-overview">
								 			<h3>Requirement</h3>
								 			<ul>
								 				<li><i class="la la-cc-visa"></i><h3>Visa type</h3><span>Student,Working
								 				</span></li>
								 				<li><i class="la la-line-chart"></i><h3>Stage</h3><span>No interview
								 				</span></li>
								 				<li><i class="la la-line-chart"></i><h3>Language level</h3><span>Intermediate
								 				</span></li>
								 			</ul>
								 		</div>
								 		<div class="job-location job-overview">
								 			<h3>Location</h3>
								 			<ul>
								 				<li><i class="la la-thumb-tack"></i><h3>Location</h3><span>Tokyo, Shinjuku, Kita-shinjuku 1-1-16</span></li>
								 			</ul>
								 			<div class="job-lctn-map">
								 				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1337.075175983414!2d106.8779449!3d47.9141282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5d9692e33ae1c475%3A0xeca42255fd89145d!2sMzm+Agency!5e0!3m2!1sen!2smn!4v1528645116170" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
								 			</div>
								 			<ul>
								 				<li><i class="la la-user"></i><h3>Nearest station</h3><span>5 min walk from Nishi-Shinjuku station</br>9 min walk from Shinjuku
								 				</span></li>
								 			</ul>
								 		</div>
								 		
							 		</div>
							 	</div>
							 </div>
					 	</div>
				 	</div>
				</div>
			</div>
		</div>
	</section>
	<?php include ('footer.php'); ?>
</div>
<?php include ('foot.php'); ?>
</body>
</html>