<!DOCTYPE html>
<html>
<?php include ('head.php'); ?>
<body>
<div class="theme-layout" id="scrollup">
	<?php include ('responsive-header.php'); ?>
	<?php include ('header.php'); ?>
	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: transparent url(../images/resource/mslider3.jpg) repeat scroll 50% -41.3px;" class="parallax scrolly-invisible no-parallax"></div>
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="ab inner-header"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block">
			<div class="container">
				 <div class="row">
				 	<div class="col-lg-2"></div>
				 	<div class="col-lg-8">
				 		<div class="about-us">
				 			<div class="row">
				 				<div class="col-lg-12"><h3>Contact us</h3></div>
				 				<div class="col-lg-12">
				 					<p>Do you need assistance using T -Baito ?Why not check out How Does It Work , located in the footer. This page provides you with Support and Help Chat to assist you through some of the important processes on TBaito website.</p>
				 					<p>Having trouble signing in?Please note, while you can search for jobs 24 hours a day, 7 days a week.... There are a number of ways to contact us:</p>
				 					<p>Please contact the T-Baito Hotline at Phone No. 12345 or at T-baitoSupport@email for</p>
				 				</div>
				 				<div class="col-lg-12 eq double-gap-bottom">
				 					<div class="edu-history style2">
			 							<i></i>
			 							<div class="edu-hisinfo">
			 								<p>help using this website</p>
			 							</div>
			 						</div>
			 						<div class="edu-history style2">
			 							<i></i>
			 							<div class="edu-hisinfo">
			 								<p>assistance with lodging vacancies</p>
			 							</div>
			 						</div>
			 						<div class="edu-history style2">
			 							<i></i>
			 							<div class="edu-hisinfo">
			 								<p>more information about the services and assistance offered to employers</p>
			 							</div>
			 						</div>
				 				</div>
				 				<div class="col-lg-12">
				 					<div class="contact-form">
				 						<h3>Send A Request</h3>
							 			<form>
							 				<div class="row">
							 					<div class="col-lg-12">
							 						<div class="pf-field">
							 							<input placeholder="Enter your name" type="text">
							 						</div>
							 					</div>
							 					<div class="col-lg-12">
							 						<div class="pf-field">
							 							<input placeholder="E-mail address" type="text">
							 						</div>
							 					</div>
							 					<div class="col-lg-12">
							 						<div class="pf-field">
							 							<input placeholder="Phone number" type="text">
							 						</div>
							 					</div>
							 					<div class="col-lg-12">
							 						<div class="pf-field">
							 							<textarea placeholder="Message should have more than 50 characters"></textarea>
							 						</div>
							 					</div>
							 					<div class="col-lg-12">
							 						<button type="submit">Send</button>
							 					</div>
							 				</div>
							 			</form>
							 		</div>
				 				</div>
				 			</div>
				 		</div>
				 	</div>
				 	<div class="col-lg-2"></div>
				 </div>
			</div>
		</div>
	</section>
	<?php include ('footer.php'); ?>
</div>
<?php include ('foot.php'); ?>
</body>
</html>