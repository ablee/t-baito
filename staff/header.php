<header class="stick-top">
		<div class="menu-sec">
			<div class="container">
				<div class="logo">
					<a href="../index.php" title="">
						<img class="hidesticky" src="../images/resource/logo.png" alt="" />
						<img class="showsticky" src="../images/resource/logo.png" alt="" />
					</a>
				</div><!-- Logo -->
				<div class="btn-extars">
					<a href="../admin/landing.php" title="" class="post-job-btn"><i class="la la-plus"></i>Post Job</a>
					<ul class="account-btns">
						<li><a title="" href="../admin/register.php"><i class="la la-user-plus"></i> Sign Up</a></li>
						<li><a title="" href="login.php"><i class="la la-sign-in"></i> Login</a></li>
					</ul>
					<nav>
						<ul>
							<li class="menu-item-has-children">
								<a href="#" title="">Language</a>
								<ul>
									<li><a href="#" title="">English</a></li>
									<li><a href="#" title="">Mongolia</a></li>
									<li><a href="#" title="">日本語</a></li>
									<li><a href="#" title="">韓国</a></li>
									<li><a href="#" title="">中文</a></li>
									<li><a href="#" title="">Russia</a></li>
									<li><a href="#" title="">Việt Nam</a></li>
									<li><a href="#" title="">O'zbek</a></li>
								</ul>
							</li>
						</ul>
					</nav>
				</div>
				<!-- Btn Extras -->
			</div>
		</div>
	</header>