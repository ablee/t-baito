<!DOCTYPE html>
<html>
<?php include ('head.php'); ?>
<body>
<div class="theme-layout" id="scrollup">
	<?php include ('responsive-header.php'); ?>
	<?php include ('header.php'); ?>
	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: transparent url(../images/resource/mslider3.jpg) repeat scroll 50% -41.3px;" class="parallax scrolly-invisible no-parallax"></div>
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header wform">
							<div class="job-search-sec">
								<div class="job-search">
									<h4>Explore Thousands of job Just a Click Away...</h4>
									<form method="get" action="category.php">
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
												<div class="job-field">
													<input type="text" placeholder="Keyword, Title, Skills..." />
													<i class="la la-search"></i> 
												</div>
											</div>
											<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
												<div class="job-field">
													<select data-placeholder="Location..." class="chosen-city">
														<option>Location...</option>
														<option>Tokyo</option>
														<option>Osaka</option>
														<option>Osaka</option>
														<option>Kyoto</option>
														<option>Kanazawa</option>
													</select>
													<i class="la la-map-marker"></i>
												</div>
											</div>
											<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
												<button type="submit">FIND JOB</button>
											</div>
										</div>
									</form>
									<div class="row">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<div class="download-sec">
												<div class="download-text">
													<ul>
														<li>
															<a href="#" title="">
																<i class="la la-apple"></i>
																<span>App Store</span>
																<p>Available now on the</p>
															</a>
														</li>
														<li>
															<a href="#" title="">
																<i class="la la-android"></i>
																<span>Google Play</span>
																<p>Get in on</p>
															</a>
														</li>
														<li>
															<a class="signup-popup" title="">
																<i class="la la-search-plus"></i>
																<span>Advenced search</span>
																<p>More in</p>
															</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block no-padding">
			<div class="container">
				 <div class="row no-gape">
				 	<aside class="col-lg-3 column border-right">
				 		<div class="widget">
				 			<h3 class="sb-title">Type</h3>
				 			<div class="specialism_widget">
				 				<div class="simple-checkbox">
									<p><input name="spealism" id="a" type="checkbox"><label for="a">Full-time</label></p>
									<p><input name="spealism" id="b" type="checkbox"><label for="b">Part-time</label></p>
									<p><input name="spealism" id="c" type="checkbox"><label for="c">Freelance</label></p>
									<p><input name="spealism" id="d" type="checkbox"><label for="d">Volunteer</label></p>
									<p><input name="spealism" id="e" type="checkbox"><label for="e">Intership</label></p>
									<p><input name="spealism" id="f" type="checkbox"><label for="f">Contractor</label></p>
									<p><input name="spealism" id="g" type="checkbox"><label for="g">Daily</label></p>
									<p><input name="spealism" id="g" type="checkbox"><label for="g">Temporary</label></p>
				 				</div>
				 			</div>
				 		</div>
				 		<div class="widget">
				 			<h3 class="sb-title">Date</h3>
				 			<div class="posted_widget" style="">
								<input name="choose" id="232" type="radio"><label for="232">Tomorrow</label><br>
								<input name="choose" id="wwqe" type="radio"><label for="wwqe">Day after Tomorrow</label><br>
								<input name="choose" id="erewr" type="radio"><label for="erewr">This week</label><br>
								<input name="choose" id="qwe" type="radio"><label for="qwe">This month</label><br>
							</div>
				 		</div>
				 		<div class="widget">
				 			<h3 class="sb-title">Shift</h3>
				 			<div class="specialism_widget">
				 				<div class="simple-checkbox">
									<p><input name="spealism" id="as" type="checkbox"><label for="as">Morning</label></p>
									<p><input name="spealism" id="bs" type="checkbox"><label for="bs">Afternoon</label></p>
									<p><input name="spealism" id="cs" type="checkbox"><label for="cs">Evening</label></p>
				 				</div>
				 			</div>
				 		</div>
				 		<div class="widget">
				 			<h3 class="sb-title">Categories</h3>
				 			<div class="specialism_widget">
				 				<div class="simple-checkbox">
									<p><input name="spealism" id="bc" type="checkbox"><label for="bc">Building & Construction </label></p>
									<p><input name="spealism" id="cc" type="checkbox"><label for="cc">Financial services
									</label></p>
									<p><input name="spealism" id="dc" type="checkbox"><label for="dc">Hospitality & Tourism services</label></p>
									<p><input name="spealism" id="ec" type="checkbox"><label for="ec">Transportation</label></p>
									<p><input name="spealism" id="fc" type="checkbox"><label for="fc">Engineering</label></p>
									<p><input name="spealism" id="gc" type="checkbox"><label for="gc">Manufacturing</label></p>
				 				</div>
				 			</div>
				 		</div>
				 		<div class="widget">
				 			<h3 class="sb-title">Salary</h3>
				 			<div class="specialism_widget">
				 				<div class="simple-checkbox">
									<p><input name="spealism" id="an" type="checkbox"><label for="an">Hourly</label></p>
									<p><input name="spealism" id="bn" type="checkbox"><label for="bn">Daily</label></p>
									<p><input name="spealism" id="cn" type="checkbox"><label for="cn">Weekly</label></p>
									<p><input name="spealism" id="dn" type="checkbox"><label for="dn">Monthly</label></p>
								</div>
				 			</div>
				 		</div>
				 		<div class="widget">
				 			<h3 class="sb-title">Location</h3>
				 			<div class="specialism_widget">
				 				<div class="simple-checkbox">
									<p><input name="spealism" id="al" type="checkbox"><label for="al">Tokyo</label></p>
									<p><input name="spealism" id="bl" type="checkbox"><label for="bl">Osaka</label></p>
									<p><input name="spealism" id="cl" type="checkbox"><label for="cl">Nagoya</label></p>
									<p><input name="spealism" id="dl" type="checkbox"><label for="dl">Kyoto</label></p>
									<p><input name="spealism" id="el" type="checkbox"><label for="el">Kanazawa</label></p>
								</div>
				 			</div>
				 		</div>
				 	</aside>
				 	<div class="col-lg-9 column">
				 		<div class="modrn-joblist">
					 		<div class="filterbar">
					 			<!--<span class="emlthis">
					 				<a href="mailto:example.com" title="">
					 					<i class="la la-envelope-o"></i> Email me Jobs Like These
					 				</a>
					 			</span>
					 			<div class="sortby-sec">
					 				<select data-placeholder="Recent job" class="chosen">
										<option>Most jobs</option>
										<option>Recent job</option>
									</select>
									<select data-placeholder="20 Per Page" class="chosen" style="display: none;">
										<option>30 Per Page</option>
										<option>40 Per Page</option>
										<option>50 Per Page</option>
										<option>60 Per Page</option>
									</select>
					 			</div>-->
					 			<h5>1000+ Jobs &amp; Vacancies</h5>
					 		</div>
						</div>
				 		<div class="emply-list-sec">
				 			<div class="emply-list">
					 			<div class="row a">
					 				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					 					<div class="title">
					 						<a href="staff/details.php">job title</a>
					 					</div>
					 				</div>
					 				<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
					 					<div class="thumb" style="background-image:url(../images/resource/b1.jpg)"></div>
					 				</div>
					 				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					 					<div class="content">
					 						<li><i class="la la-money"></i>Salary</li>
					 						<li><i class="la la-map-marker"></i>Location</li>
					 						<li><i class="la la-clock-o"></i>Shift</li>
					 					</div>
					 				</div>
					 				<div class="col-lg-5 col-md-4 col-sm-12 col-xs-12">
					 					<div class="base">
					 						Company Name / Daily Job
					 					</div>
					 				</div>
					 				<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
					 					<div class="job-but">
					 						<a href="staff/details.php"><span class="apply">Apply</span></a>
											<i><a href="staff/details.php">End date</a></i>
										</div>
					 				</div>
					 			</div>
					 		</div>
					 		<div class="emply-list">
					 			<div class="row a">
					 				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					 					<div class="title">
					 						<a href="staff/details.php">job title</a>
					 					</div>
					 				</div>
					 				<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
					 					<div class="thumb" style="background-image:url(../images/resource/b1.jpg)"></div>
					 				</div>
					 				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					 					<div class="content">
					 						<li><i class="la la-money"></i>Salary</li>
					 						<li><i class="la la-map-marker"></i>Location</li>
					 						<li><i class="la la-clock-o"></i>Shift</li>
					 					</div>
					 				</div>
					 				<div class="col-lg-5 col-md-4 col-sm-12 col-xs-12">
					 					<div class="base">
					 						Company Name / Daily Job
					 					</div>
					 				</div>
					 				<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
					 					<div class="job-but">
					 						<a href="staff/details.php"><span class="apply">Apply</span></a>
											<i><a href="staff/details.php">End date</a></i>
										</div>
					 				</div>
					 			</div>
					 		</div>
					 		<div class="emply-list">
					 			<div class="row a">
					 				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					 					<div class="title">
					 						<a href="staff/details.php">job title</a>
					 					</div>
					 				</div>
					 				<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
					 					<div class="thumb" style="background-image:url(../images/resource/b1.jpg)"></div>
					 				</div>
					 				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					 					<div class="content">
					 						<li><i class="la la-money"></i>Salary</li>
					 						<li><i class="la la-map-marker"></i>Location</li>
					 						<li><i class="la la-clock-o"></i>Shift</li>
					 					</div>
					 				</div>
					 				<div class="col-lg-5 col-md-4 col-sm-12 col-xs-12">
					 					<div class="base">
					 						Company Name / Daily Job
					 					</div>
					 				</div>
					 				<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
					 					<div class="job-but">
					 						<a href="staff/details.php"><span class="apply">Apply</span></a>
											<i><a href="staff/details.php">End date</a></i>
										</div>
					 				</div>
					 			</div>
					 		</div>
					 		<div class="emply-list">
					 			<div class="row a">
					 				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					 					<div class="title">
					 						<a href="staff/details.php">job title</a>
					 					</div>
					 				</div>
					 				<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
					 					<div class="thumb" style="background-image:url(../images/resource/b1.jpg)"></div>
					 				</div>
					 				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					 					<div class="content">
					 						<li><i class="la la-money"></i>Salary</li>
					 						<li><i class="la la-map-marker"></i>Location</li>
					 						<li><i class="la la-clock-o"></i>Shift</li>
					 					</div>
					 				</div>
					 				<div class="col-lg-5 col-md-4 col-sm-12 col-xs-12">
					 					<div class="base">
					 						Company Name / Daily Job
					 					</div>
					 				</div>
					 				<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
					 					<div class="job-but">
					 						<a href="staff/details.php"><span class="apply">Apply</span></a>
											<i><a href="staff/details.php">End date</a></i>
										</div>
					 				</div>
					 			</div>
					 		</div>
					 		<div class="emply-list">
					 			<div class="row a">
					 				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					 					<div class="title">
					 						<a href="staff/details.php">job title</a>
					 					</div>
					 				</div>
					 				<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
					 					<div class="thumb" style="background-image:url(../images/resource/b1.jpg)"></div>
					 				</div>
					 				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					 					<div class="content">
					 						<li><i class="la la-money"></i>Salary</li>
					 						<li><i class="la la-map-marker"></i>Location</li>
					 						<li><i class="la la-clock-o"></i>Shift</li>
					 					</div>
					 				</div>
					 				<div class="col-lg-5 col-md-4 col-sm-12 col-xs-12">
					 					<div class="base">
					 						Company Name / Daily Job
					 					</div>
					 				</div>
					 				<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
					 					<div class="job-but">
					 						<a href="staff/details.php"><span class="apply">Apply</span></a>
											<i><a href="staff/details.php">End date</a></i>
										</div>
					 				</div>
					 			</div>
					 		</div>
					 		<div class="emply-list">
					 			<div class="row a">
					 				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					 					<div class="title">
					 						<a href="staff/details.php">job title</a>
					 					</div>
					 				</div>
					 				<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
					 					<div class="thumb" style="background-image:url(../images/resource/b1.jpg)"></div>
					 				</div>
					 				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					 					<div class="content">
					 						<li><i class="la la-money"></i>Salary</li>
					 						<li><i class="la la-map-marker"></i>Location</li>
					 						<li><i class="la la-clock-o"></i>Shift</li>
					 					</div>
					 				</div>
					 				<div class="col-lg-5 col-md-4 col-sm-12 col-xs-12">
					 					<div class="base">
					 						Company Name / Daily Job
					 					</div>
					 				</div>
					 				<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
					 					<div class="job-but">
					 						<a href="staff/details.php"><span class="apply">Apply</span></a>
											<i><a href="staff/details.php">End date</a></i>
										</div>
					 				</div>
					 			</div>
					 		</div>
					 		<div class="emply-list">
					 			<div class="row a">
					 				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					 					<div class="title">
					 						<a href="staff/details.php">job title</a>
					 					</div>
					 				</div>
					 				<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
					 					<div class="thumb" style="background-image:url(../images/resource/b1.jpg)"></div>
					 				</div>
					 				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					 					<div class="content">
					 						<li><i class="la la-money"></i>Salary</li>
					 						<li><i class="la la-map-marker"></i>Location</li>
					 						<li><i class="la la-clock-o"></i>Shift</li>
					 					</div>
					 				</div>
					 				<div class="col-lg-5 col-md-4 col-sm-12 col-xs-12">
					 					<div class="base">
					 						Company Name / Daily Job
					 					</div>
					 				</div>
					 				<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
					 					<div class="job-but">
					 						<a href="staff/details.php"><span class="apply">Apply</span></a>
											<i><a href="staff/details.php">End date</a></i>
										</div>
					 				</div>
					 			</div>
					 		</div>
					 		<div class="emply-list">
					 			<div class="row a">
					 				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					 					<div class="title">
					 						<a href="staff/details.php">job title</a>
					 					</div>
					 				</div>
					 				<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
					 					<div class="thumb" style="background-image:url(../images/resource/b1.jpg)"></div>
					 				</div>
					 				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					 					<div class="content">
					 						<li><i class="la la-money"></i>Salary</li>
					 						<li><i class="la la-map-marker"></i>Location</li>
					 						<li><i class="la la-clock-o"></i>Shift</li>
					 					</div>
					 				</div>
					 				<div class="col-lg-5 col-md-4 col-sm-12 col-xs-12">
					 					<div class="base">
					 						Company Name / Daily Job
					 					</div>
					 				</div>
					 				<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
					 					<div class="job-but">
					 						<a href="staff/details.php"><span class="apply">Apply</span></a>
											<i><a href="staff/details.php">End date</a></i>
										</div>
					 				</div>
					 			</div>
					 		</div>
				 			<div class="pagination">
								<ul>
									<li class="prev"><a href=""><i class="la la-long-arrow-left"></i></a></li>
									<li><a href="">1</a></li>
									<li class="active"><a href="">2</a></li>
									<li><a href="">3</a></li>
									<li><span class="delimeter">...</span></li>
									<li><a href="">14</a></li>
									<li class="next"><a href=""><i class="la la-long-arrow-right"></i></a></li>
								</ul>
							</div><!-- Pagination -->
				 		</div>
					</div>
				 </div>
			</div>
		</div>
	</section>
	<?php include ('footer.php'); ?>
</div>
<?php include ('foot.php'); ?>
</body>
</html>

