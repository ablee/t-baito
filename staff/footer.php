<footer>
	<div class="block">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 column">
					<div class="widget">
						<div class="about_widget">
							<div class="logo">
								<a href="../index.php" title=""><img src="../images/resource/logo.png" alt="" /></a>
							</div>
							<span>192-0063</span>
							<span>東京都新宿区北新宿1-1-16　JSビル</span>
							<span>+81 03 2222 3333</span>
							<span>info@t-batio.co</span>
						</div><!-- About Widget -->
					</div>
				</div>
				<div class="col-lg-3 column">
					<div class="widget">
						<h3 class="footer-title">Links</h3>
						<div class="link_widgets">
							<div class="row">
								<div class="col-lg-12">
									<a href="policy.php" title="">Privacy & Seurty </a>
									<a href="term-of-use.php" title="">Terms of Use</a>
									<a href="about.php" title="">About us</a>
									<a href="support.php" title="">Support</a>
									<a href="contact.php" title="">Contact Us</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-2 column">
					<div class="widget">
						<h3 class="footer-title">Follow Us</h3>
						<div class="link_widgets3 nolines">
							<div class="row">
								<div class="col-lg-12">
									<a href="#" title=""><i class="fa fa-facebook"></i> Facebook</a>	
									<a href="#" title=""><i class="fa fa-twitter"></i> Twitter</a>	
									<a href="#" title=""><i class="fa fa-instagram"></i> Instagram</a>
									<a href="#" title=""><i class="fa fa-behance"></i> Behance</a>	
									<a href="#" title=""><i class="fa fa-google-plus"></i> Google</a>	
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 column">
					<div class="widget">
						<div class="download_widget">
							<a href="#" title=""><img src="../images/resource/dw1.png" alt="" /></a>
							<a href="#" title=""><img src="../images/resource/dw2.png" alt="" /></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="bottom-line">
		<div class="container">
			<span>© 2018 Japan Project</span>
			<a href="#scrollup" class="back-top" title=""><i class="la la-long-arrow-up"></i></a>
		</div>
	</div>
</footer>