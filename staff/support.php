<!DOCTYPE html>
<html>
<?php include ('head.php'); ?>
<body>
<div class="theme-layout" id="scrollup">
	<?php include ('responsive-header.php'); ?>
	<?php include ('header.php'); ?>
	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: transparent url(../images/resource/mslider3.jpg) repeat scroll 50% -41.3px;" class="parallax scrolly-invisible no-parallax"></div>
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="ab inner-header"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block">
			<div class="container">
				 <div class="row">
				 	<div class="col-lg-12">
				 		<div class="about-us">
				 			<div class="row">
				 				<div class="col-lg-12 faq-box job-details">
				 					<h2>Support and How to use the page</h2>
				 					<p>We offer 24/7 support in a way that is convenient for you. TemplateMonster Support Team will give you quick and professional support and answer all your questions. You will ﬁnd information on our products and services at this page.</p>
				 					<h2>FAQ</h2>
				 					<h2>How do I delete my proﬁle?</h2>
				 					<p>You can delete any information from your proﬁle as needed. Or, if you would like to delete your entire proﬁle you can <a href="#">contact us.</a></p>
				 					<h2>Reset my password</h2>
				 					<p>Please follow the steps below to easily reset the password to your account.</p>
				 					<ul>
				 						<li>ClickSign Inlocated at the top right of the page</li>
				 						<li>ClickForgot Password.</li>
				 						<li>Enter the email address associated with your account.</li>
				 						<li>ClickReset.</li>
				 						<li>Within 15 minutes you will receive an email including instructions on how to reset your password.</li>
				 					</ul>
				 					<p>Note: The link that you receive is only valid for 24 hours. If you do not access this link within 24 hours, you will need to request a new link.</p>
				 					<ul>
				 						<li>If you do not receive the reset password email after 30 minutes, please check your spam folder before a second attempt.</li>
				 					</ul>
				 					<p>You may also change your password at anytime, within your Account Settings.</p>
				 					<ul>
				 						<li>Sign in to T-baito</li>
				 						<li>Click the proﬁle icon.</li>
				 						<li>ClickAccount in the drop down menu.</li>
				 						<li>Scroll down to thePassword Resetmodule</li>
				 						<li>Enter your updated password in theNew Passwordﬁeld.</li>
				 						<li>Type your updated password again in theConﬁrm New Passwordﬁeld.</li>
				 						<li>Click Save Changes.</li>
				 					</ul>
				 					<h2>How do I access and create my proﬁle on T-Baito website </h2>
				 					<p>Currently there are two ways to access your proﬁle on T-baito Website. First is via the person icon in the header. Second is to access it via the Website homepage for select Sign Up</p>
				 					<h2>Will my proﬁle be searchable through Google or another search engine</h2>
				 					<p>No, at this time, we do not make proﬁles ﬁndable via search engines. If this should ever change, we will share updates and insights on how you can manage your proﬁle.</p>
				 					<h2>Does a proﬁle connect to any reviews, salary or other user-generated insight I have provided?</h2>
				 					<p>No, any personal information included in your proﬁle is separate from anonymous reviews or data you may have contributed. That information is and always will remain anonymous on T-baito.</p>
				 					<h2>What are the differences between viewing as myself or my employer view?</h2>
				 					<p>The employer view allows you to see what an employer may see when viewing your proﬁle. It removes any unﬁlled sections on your T-baito proﬁle and any of the administrative controls that allow you to modify your proﬁle. As of today, however, we are not currently providing access to proﬁles to employers at scale, but we may periodically test product updates and features with employers.</p>
				 					<p>The 'myself' view shows you all sections of the T-baito proﬁle that you can update at this time along with admin controls to make updates and edits.</p>
				 					<h2>Are proﬁles viewable by employers?</h2>
				 					<p>While we are not currently providing access to proﬁles to employers at scale, we may periodically test product updates and features with employers. Also important is that we will not share any anonymous insights (i.e. reviews, ratings, salary reports, ofﬁce photos) you have shared on T-Baito, and any personally identiﬁable information.</p>
				 					<p>jobs and companies (i.e. reviews, salary reports, etc).</p>
				 				</div>
				 			</div>
				 		</div>
				 	</div>
				 </div>
			</div>
		</div>
	</section>
	<?php include ('footer.php'); ?>
</div>
<?php include ('foot.php'); ?>
</body>
</html>