<!DOCTYPE html>
<html>
<?php include ('head.php'); ?>
<body>
<div class="theme-layout" id="scrollup">
	<?php include ('responsive-header.php'); ?>
	<?php include ('header.php'); ?>
	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: transparent url(../images/resource/mslider3.jpg) repeat scroll 50% -41.3px;" class="parallax scrolly-invisible no-parallax"></div>
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="ab inner-header"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block">
			<div class="container">
				 <div class="row">
				 	<div class="col-lg-12">
				 		<div class="about-us">
				 			<div class="row">
				 				<div class="col-lg-12"><h3>About us</h3></div>
				 				<div class="col-lg-12">
				 					<p><strong>Mission:</strong> To become a bridge connecting job seekers and headhunters </p>
				 					<p><strong>Company:</strong><br>
				 						Tab-Solution Ltd.Co. is a newly start-up company based in Tokyo. With its unique insights to end-to-end human capital solutions, the company is expected to help the recruitment go through the entire processes of searching, matching and managing in a simple and comprehensive way in response to the huge shortage of labor force in Japan and complicated recruitment procedures of Japanese working culture.
 									</p>
				 				</div>
				 				<div class="col-lg-12">
				 					<p>In order to completely satisfy our clients and candidates, our system holds a growing database of millions of company reviews, candidates' resumes, interview reviews, recruitment resources and more. Such database helps millions of people find jobs and hundreds of thousands of employers to find, hire and manage the great talent they need. Add to that millions of the latest jobs — no other site allows you to see which employers are hiring, what it's really like to work or interview there according to employees, and how much you could earn. We are also available via mobile app on iOS and Android platforms under the name T-Baito for staffs and T-Baito Mananger for recruiters.</p>
				 				</div>
				 				<div class="col-lg-12">
				 					<p>T-Baito puts the job-finding power in the palm of your hand to get hired easily without any language barrier with the interfaces available in English, Japanese, Mongolian and Vietnamese, and another 25 languages under development, and a wide range of features which provide a full listing of job categories, your work schedule and salary data.</p>
				 					<p><strong>Apps' Key features:</strong><br>Find job openings nearest to you by using your device’s GPS</p>
				 					<p>Provide 1 day-job to maximize your spare-time and fulfill your every single day</p>
				 					<p>Get hired immediately by contacting your potential companies directly </p>
				 					<p>Save your job-search history and personal profile for future job application</p>
				 					<p>View and check daily salary and working schedule</p>
				 					<p>Manage your work shift </p>
				 					<p><strong>Leaderships:</strong><br>Bio of BOD</p>
				 					<p><strong>NEWS & RESOURCES</strong></p>
				 					<p><strong>Job search resources:</strong> Being prepared to ge through the unemployment period, Top 10 job interviews,  Tips for next job interview</p>
				 					<p><strong>Recruitment advices:</strong> Career planning tips, candidate tracking systems</p>
				 					<p><strong>Latest News:</strong> What's new?</p>
				 					<p>Office Life Topics, The Future of Work</p>
				 					<p><strong>FEATURES</strong></p>
				 					<p><strong>Search million jobs</strong></p>
				 					<p>Quickly and easily find open positions tailored for you and fitting your skills and experience. We provide daily job to maximize your spare-time and fulfill your every single day </p>
				 					<p><strong>Discover local jobs</strong></p>
				 					<p>See who's hiring in your area and find a job nearest to you by using your device’s GPS in the most popular industries, top companies and job types => Learn more</p>
				 					<p><strong>Get hired immediately </strong></p>
				 					<p>Create an account and upload your resume to be found by the recruiters who search T-Baito's databases every day. 
Our online form makes it easy to send us your resume or upload the details of your social networks. 
We’ll then contact you if your qualifications meet the requirements of an open position or match what our clients typically look for.
</p>
				 					<p><strong>Smart matches</strong></p>
				 					<p>Connect Job seekers & Headhunters! It’s all coming together!!! We use great search filters, smart algorithms and relevant alerts to get the best job matches to your computer or phone.we'll send an alert when a good fit for you is posted.
Make your resume public to companies there in less than a minute!  Explore how we make it!</p>
				 					<p><strong>Mobile Apps</strong></p>
				 					<p>
Find job on the go
Search for jobs on your mobile. The T-Baito apps bring over thousands of jobs to your fingertips. Receive job alerts and never miss a job opportunity any where and any time; Download our free mobile apps</p>
				 					<p><strong>HR systems</strong></p>
				 					<p>Manage your candidates in hand!</p>
				 					<blockquote><p><i>“</i><span>T-Baito has millions of jobs plus salary information and company reviews making it easy to find a job that is a right fit for you.<i>”</i></span></p></blockquote>
				 					<p>NEED JOB SEEKING ADVICES? </p>
				 					<p>WE ARE HERE TO HELP YOU!</p>
				 					<p>Making successful applications <a class="in" href="#">More</a></p>
				 					<p>Managing the interview <a class="in" href="#">More</a></p>
				 					<p>Tips for next job interview <a class="in" href="#">More</a></p>
				 				</div>
				 			</div>
				 		</div>
				 	</div>
				 </div>
			</div>
		</div>
	</section>
	<section>
		<div class="block remove-top">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="our-services">
							<div class="row">
								<div class="col-lg-12"><h2>Our history</h2></div>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="service">
										<div class="edu-history style2">
				 							<i></i>
				 							<div class="edu-hisinfo">
				 								<h3>We are a development and manufacturing organization producing antenna, alignment tools and antenna monitoring systems to support a variety of industries</h3>
				 							</div>
				 						</div>
				 						<div class="edu-history style2">
				 							<i></i>
				 							<div class="edu-hisinfo">
				 								<h3>The founder of our company have a combined 10+ years of engineering and management experience  , and are uniquely equipped to provide innovative solutions to industry problems.</h3>
				 							</div>
				 						</div>
				 						<div class="edu-history style2">
				 							<i></i>
				 							<div class="edu-hisinfo">
				 								<h3>The Company was founded in 2017. Over these years the company has earned a reputation that has a unique combination of quality, value , trust and reliability .</h3>
				 							</div>
				 						</div>
				 						<div class="edu-history style2">
				 							<i></i>
				 							<div class="edu-hisinfo">
				 								<h3>Our headquarters are located in Tokyo, Japan</h3>
				 							</div>
				 						</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php include ('footer.php'); ?>
</div>
<?php include ('foot.php'); ?>
</body>
</html>