<!DOCTYPE html>
<html>
<?php include ('head.php'); ?>
<body>
<div class="theme-layout" id="scrollup">
	<?php include ('responsive-header.php'); ?>
	<?php include ('header.php'); ?>
	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: transparent url(../images/resource/mslider3.jpg) repeat scroll 50% -41.3px;" class="parallax scrolly-invisible no-parallax"></div>
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="ab inner-header"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block">
			<div class="container">
				 <div class="row">
				 	<div class="col-lg-12">
				 		<div class="about-us">
				 			<div class="row">
				 				<div class="col-lg-12"><h3>Terms and Conditions</h3></div>
				 				<div class="col-lg-12 faq-box job-details">
				 					<p>This page is the Terms of Use ("Terms") under which you may use T-Baito (www.t-baito.com or mobile app T-Baito). Please read these Terms carefully. If you do not accept the Terms stated here, do not use T-Baito and its services. By using T-Baito, you agree to be bound by these Terms. T-Baito may revise these Terms at any time by posting an updated version to this Web page or mobile app. You should visit check periodically to review the most current Terms because they are binding on you.</p>
				 					<h2>1. General provision</h2>
				 					<ul>
 										<li>T-Baito provides you an online service to post and search employment opportunities and user resumes. It also allows Users to create individual proﬁles, which may include personal information.</li>
 										<li>You must be 15 years of age or older to visit or use T-Baito in any manner. By visiting T-Baito or accepting these Terms, you represent and warrant to T-Baito that you have reached the age of majority in your jurisdiction, and that you have the right, authority and capacity to agree to and abide by these Terms. You also represent and warrant to T-Baito that you will use T-Baito in a manner consistent with any and all applicable laws and regulations.</li>
 										<li>Member is a person or legal entity who did an online registration through this site or mobile app and permitted as a member by T-Baito.</li>
 										<li>User is a person or legal entity who are visiting and using this service.(including the members)</li>
 										<li> This terms bound all of the relations which arise between T-Baito and Users.
 										</li>
 									</ul>
 									<h2>2. Registration Information</h2>
 									<p>2.1 When you register with T-Baito, you will be asked to create a T-Baito Account and provide T-Baito with certain information including, without limitation, a valid email address.</p>
 									<p>2.2. T-Baito may not be able to respond to inquiries when such information is incomplete or unclear. T-Baito  strictly prohibits the provision of false contact information, or information belonging to that of another party.</p>
 									<h2>3. ID and Password</h2>
 									<p>1. You will be given a ID and password by T-baito. Members shall bear all responsibility for the use and management of their ID and password.</p>
 									<p>2. You may not share your password or other account access information with any other party, temporarily or permanently, and you shall be responsible for all uses of your registrations and passwords, whether or not authorized by you. You agree to immediately notify T-Baito of any unauthorized use of your account, proﬁle, or passwords.</p>
 									<p>3. Because the management of the member's ID and password is insufﬁcient, members shall be responsible for any damages caused by being used by third parties.</p>
 									<h2>4. Privacy policy</h2>
 									<p>Information you submit will be used in accordance with T-Baito' Privacy Policies.</p>
 									<h2>5. User responsibility</h2>
 									<ul>
 										<li>1. Users shall use T - Baito on their own will and bear all responsibility for use.</li>
 										<li>2. You are responsible for your account information, employer account information or job postings, Proﬁle, content, messages, audio, video, photos, text, images, compilations or other information ("User Content") that you post on T-Baito or transmit to other Users.</li>
 										<li>3. The registration information set forth in the preceding paragraph shall be able to be changed, added or deleted by the members themselves at any time within the scope of the service contents provided by this service and the members shall always be kept accurate, complete and up to date in accordance with the purpose of use.</li>
 										<li>4. Because there is a possibility that data in T - BAITO may be erased / changed, the user is responsible for the user 's own information.</li>
 									</ul>
 									<h2>6. Copyrights</h2>
 									<p>6.1. T-Baito authorizes you to view and access a single copy of the content available on or from T-Baito solely for your personal use. The contents of T-Baito, such as text, graphics, images, logos, button icons, software and other TBaito content, are protected under both Japan and foreign copyright, trademark and other laws. You agree not to sell or modify the T-Baito content or reproduce, display, publicly perform, distribute, or otherwise use the T-Baito content in any way for any public or commercial purpose, in connection with products or services that are not those of TBaito, in any other manner that is likely to cause confusion among consumers, that disparages or discredits T-Baito or its licensors, that dilutes the strength of T-Baito', or that otherwise infringes T-Baito' intellectual property rights. TBaito and all of its afﬁliated companies respect the intellectual property of others, and we ask our Users and content partners to do the same. The unauthorized posting, reproduction, copying, distribution, modiﬁcation, public display or public performance of copyrighted works constitutes infringement of the copyright owner's rights.</p>
 									<p>6.2. In the event that a user violates the provisions of this section and a dispute concerning intellectual property rights such as copyright, trademark right occurs, the user solves the problem at his / her own expense and responsibility.</p>
 									<h2>7. Restriction</h2>
 									<p>7.1. Users may not use T-Baito in order to transmit, post, distribute, store or destroy material, including without limitation:</p>
 									<ul>
 										<li>1. that is defamatory, obscene, threatening, abusive or hateful.</li>
 										<li>2. in a manner that will infringe the copyright, trademark, trade secret or other intellectual property rights of others or violate the privacy, publicity or other personal rights of others,</li>
 										<li>3. Attempting to violate the system or network security, any other attempts to prevent the use of T-baito</li>
 										<li>4. To transfer the information obtained by this service to a third party beyond the scope of the purpose of using this service, or to provide information for proﬁt purposes</li>
 										<li>5. Registering or providing false information</li>
 										<li>6. Action against public order and morals</li>
 										<li>7. in violation of any applicable law or regulation,</li>
 										<li>8. Any other act that we deem inappropriate</li>
 									</ul>
 									<p>7.2. In violation of the above and in case of damage to our company, the user agrees in advance that we can request damages against the user.</p>
 									<h2>8. Limitation of Liability</h2>
 									<p>8.1. T-Baito makes no warranties about the accuracy, reliability, completeness, or timeliness of the T-baito content, services, software, text, graphics, and links. Also T-Baito offers no guarantees whatsoever that the Website functions will not be interrupted, that no errors will occur, that no computer viruses or other harmful content is contained in the Website or Mobile app and its servers.</p>
 									<p>8.2. T-Baito assumes no responsibility for any damages incurred by the user when using T-Baito and its content, excluding any intent or gross negligence on the part of the Company.</p>
 									<p>1) damages which is caused between users and third parties</p>
 									<p>2) T-Baito assumes no responsibility for viruses not preventable through normally instituted virus countermeasures, natural disasters, accident of power failure, communication line or any other damages that occur due to circumstances not attributable to our company</p>
 									<p>3) Any other damages caused by reasons not dependent on our company</p>
 									<h2>9. Changes and interruption, cancellation</h2>
 									<p>The information, ﬁle names, or T-Baito itself may be changed, interrupted temporarily or long term, or deleted without prior notice.</p>
 									<h2>10. Changes of the Terms of use</h2>
 									<p>Our company may alter, add to, delete from, or make other changes to these Terms of Use; accordingly, users should always verify the latest terms in effect.</p>
 									<h2>11. Company Response</h2>
 									<p>We will terminate the accounts of any users, and block access to the site/mobile app of any users who are in a violation of the Terms of Usage. We reserve the right to take these actions at any time, in our sole discretion, with or without notice, and without any liability to the user who is terminated or to the user whose access is blocked.</p>
 									<h2>12. Compensation</h2>
 									<p>If the user violates these terms and damages to us, the user will be liable for damages against us.</p>
 									<h2>13. Dispute</h2>
 									<p>You will resolve any claim, cause of action or dispute (claim) you have with us arising out of or relating to this term or T-Baito exclusively in the Tokyo district court.</p>
				 				</div>
				 			</div>
				 		</div>
				 	</div>
				 </div>
			</div>
		</div>
	</section>
	<?php include ('footer.php'); ?>
</div>
<?php include ('foot.php'); ?>
</body>
</html>