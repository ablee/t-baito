<!DOCTYPE html>
<html>
<?php include ('head.php'); ?>
<body>
<div class="theme-layout" id="scrollup">
	<?php include ('responsive-header.php'); ?>
	<?php include ('header.php'); ?>
	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: transparent url(../images/resource/mslider3.jpg) repeat scroll 50% -41.3px;" class="parallax scrolly-invisible no-parallax"></div>
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="ab inner-header"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="block remove-bottom">
			<div class="container">
				<div class="row">
					<div class="about-us col-lg-12 text-center"><h3>Sign in to T-BAITO</h3></div>
					<div class="col-lg-12 mb50">
						<div class="account-popup-area signin-popup-box static">
							<div class="account-popup">
								<form>
									<div class="cfield">
										<input placeholder="E-mail address" type="text">
										<i class="la la-user"></i>
									</div>
									<div class="cfield">
										<input placeholder="********" type="password">
										<i class="la la-key"></i>
									</div>
									<p class="remember-label">
										<input name="cb" id="cb1" type="checkbox"><label for="cb1">Remember me</label>
									</p>
									<a href="#" title="">Forgot Password?</a>
									<button type="submit">SIGN IN</button>
								</form>
							</div>
						</div><!-- LOGIN POPUP -->
					</div>
					<div class="col-lg-12 mb50 text-center log-a">
						<a href="support.php">Support</a>
						<a href="policy.php">Privacy Policy</a>
						<a href="term-of-use.php">Terms of Use</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php include ('footer.php'); ?>
</div>
<?php include ('foot.php'); ?>
</body>
</html>